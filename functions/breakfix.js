	'use strict';

const functions = require('firebase-functions');
const dialogflowFulfillment = require('./dialogflow-fulfillment-builder');


const df_fulfillment = require('./state-machine/fulfillment');



const config = {
    'platformsEnabled': ['TEXT'] // define the platforms to be supported as per requirement
};


/********* Prompts start **************************/

var prompts_950_I_1 = 
{
	'en':'<speak>If your serial number is a mixture of letters and numbers press 1. If it’s only numbers press 2.</speak>',
	'es':'<speak>Si su número de serie es una mezcla de letras y números, púlse 1. Si son solo números, púlse 2.</speak>',
	'sv':'<speak>Om ert serienummer är en blandning av bokstäver och siffror, tryck 1. Om det består av endast siffror, tryck 2.</speak>',
	'da':'<speak>Hvis dit serienummer er en blandning af bogstaver og numre, tast 1. Hvis det kun (består af) er numre, tast 2.</speak>',
	'no':'<speak>Hvis ditt serienummer inneholder en blanding av bokstaver og tall, trykk 1. Hvis det er bare tall, trykk 2.</speak>',
	'nl':'<speak>Als uw serienummer een combinatie van letters en cijfers is, drukt u op 1. Als het alleen cijfers zijn, drukt u op 2</speak>',
	'pt':'<speak>Se o seu número de série contém letras e números prima 1. Se contem apenas números prima 2.</speak>'
};



var prompts_2100_I_1 = 
{
	'en':'<speak>Is your serial number a mixture of letters and numbers? Say yes or press 1. If not say no or press 2</speak>',
	'es':'<speak>¿Su número de serie es una mezcla de letras y números? Diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Är ert serienummer en blandning av bokstäver och siffror? Säg ja eller tryck 1. Om inte, säg nej eller tryck 2.</speak>',
	'da':'<speak>Er dit serienummer en blandning af bogstaver og numre? Sig ja eller tast 1. Hvis ikke, sig nej eller tast 2.</speak>',
	'no':'<speak>Består ditt serienummer serienummer av en blanding av bokstaver og tall? Si Ja eller tast 1. Hvis ikke si Nei eller tast 2.</speak>',
	'nl':'<speak>Is uw serienummer een combinatie van letters en cijfers? Zeg ja of druk op 1. Zo nee, zeg nee of druk op 2</speak>',
	'pt':'<speak>O seu número de série contém letras e números? Diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};

var prompts_2200_I_1 = 
{
	'en':'<speak>In order to assist you I need to get the serial number of your machine. Please tell me that number now.</speak>',
	'es':'<speak>Para poder ayudarle, necesito el número de serie de su máquina. Dígame ese número ahora.</speak>',
	'sv':'<speak>För att kunna hjälpa er behöver jag serienumret eller maskinnumret för er maskin. Vänligen meddela numret nu.</speak>',
	'da':'<speak>for at kunne hjaelpe dig, skal jeg bruge serienummeret på din maskine. Venligst fortael mig seriennummeret nu.</speak>',
	'no':'<speak>For å kunne bistå deg, så trenger jeg serienummer for din maskin. Vennligst fortell meg dette tallet nå.</speak>',
	'nl':'<speak>Om u te helpen, heb ik het serienummer van uw machine nodig. Kunt u dat aan mij doorgeven?</speak>',
	'pt':'<speak>Para o poder ajudar necessitamos do número de série da sua máquina. Diga por favor esse número.</speak>'
};

var prompts_2200_confirm = 
{
	'en':'<speak>That was <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  If that\'s correct say yes or press 1. Otherwise say no or press 2.</speak>',
	'es':'<speak>Eso fue  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Si eso es correcto, diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Om detta stämmer, säg ja eller tryck 1. Annars säg nej eller tryck 2.</speak>',
	'da':'<speak>Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Hvis dette er korrekt, sig ja eller tast 1. Ellers, sig nej eller tast 2.</speak>',
	'no':'<speak>Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Hvis dette stemmer, si ja eller tast 1. Ellers si ja eller tast 2.</speak>',
	'nl':'<speak>Dat was <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>',
	'pt':'<speak>Indicou  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Se está correto diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};

var prompts_2650_I_1 = 
{
	'en':'<speak>Please say or enter your serial number now.</speak>',
	'es':'<speak>Por favor, diga o escriba su número de serie ahora.</speak>',
	'sv':'<speak>Vänligen säg eller lägg in ert serienummer nu.</speak>',
	'da':'<speak>Venligst indtal eller tast dit serienummer nu.</speak>',
	'no':'<speak>Vennligst si eller tast inn ditt serienummer nå.</speak>',
	'nl':'<speak>Zeg of voer uw serienummer in</speak>',
	'pt':'<speak>Diga ou introduza o seu número de série.</speak>'
};


var prompts_2650_disconfirm = 
{
	'en':'<speak>Please say or enter your serial number now.</speak>',
	'es':'<speak>Por favor, usando su teclado escriba su número de serie.</speak>',
	'sv':'<speak>Vänligen säg eller lägg in ert serienummer nu.</speak>',
	'da':'<speak>Venligst indtal eller tast dit serienummer nu.</speak>',
	'no':'<speak>Vennligst si eller tast inn ditt serienummer nå.</speak>',
	'nl':'<speak>Zeg of voer uw serienummer in</speak>',
	'pt':'<speak>Diga ou introduza o seu número de série.</speak>'
};

var prompts_2650_NM_1 = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Using your keypad please enter  your serial number.</speak>',
	'es':'<speak>Lo siento, no entendí eso. Usando su teclado, por favor escriba su número de serie.</speak>',
	'sv':'<speak>Förlåt, det uppfattades inte. Vänligen använd knapparna på telefonen och lägg in ert serienummer.</speak>',
	'da':'<speak>Beklager, det jeg forstod ikke. Indtast dit serienummer ved brug af det numeriske tastatur.</speak>',
	'no':'<speak>Beklager, jeg forstod ikke dette. Vennligst bruk nummertastene for å legge inn ditt serienummer.</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet heb begrepen. Voer met behulp van uw toetsenbord uw serienummer in.</speak>',
	'pt':'<speak>Desculpe, não entendi. Usando o seu teclado, por favor introduza o seu número de série.</speak>'
};

var prompts_2650_NI_1 = 
{
	'en':'<speak>Using your keypad please enter your serial number.</speak>',
	'es':'<speak>Usando su teclado, por favor escriba su número de serie.</speak>',
	'sv':'<speak>Genom att använda knapparna på telefonen, vänligen lägg in serienumret.</speak>',
	'da':'<speak>Indtast dit serienummer ved brug af de numeriske tastatur.</speak>',
	'no':'<speak>Ved å bruke nummertastene, vennligst legg inn ditt serienummer.</speak>',
	'nl':'<speak>Voer met behulp van uw toetsenbord uw serienummer in.</speak>',
	'pt':'<speak>Usando o seu teclado, por favor introduza o seu número de série.</speak>'
};


var prompts_2650_confirm = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  If that\'s correct say yes or press 1. Otherwise say no or press 2.</speak>',
	'es':'<speak>Obtuve  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Si eso es correcto, diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Det är <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Om detta stämmer, säg ja eller tryck 1. Annars säg nej eller tryck 2.</speak>',
	'da':'<speak>Jeg modtog <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Hvis dette er korrekt, sig ja eller tast 1. Ellers, sig nej eller tast 2.</speak>',
	'no':'<speak>Jeg fikk <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Hvis dette stemmer, si ja eller tast 1. Ellers si ja eller tast 2.</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>',
	'pt':'<speak>Indicou  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Se está correto diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};


var prompts_2500_NM_1 = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Please tell me the first 3 characters of your serial number one at a time, for example F B T.</speak>',
	'es':'<speak>Lo siento, no entendí eso. Por favor, dígame los 3 primeros caracteres de su número de serie, uno a la vez, por ejemplo F B T.</speak>',
	'sv':'<speak>Förlåt, det uppfattades inte. Vänligen meddela mig de första tre tecknen i ert serienummer, ett i taget, till exempel F B T.</speak>',
	'da':'<speak>Beklager, det jeg forstod ikke. Venglist indtal de förste 3 tegn af dit serienummer, en af gangen, for eksempel F B T.</speak>',
	'no':'<speak>Beklager, jeg forstod ikke dette. Vennligst fortell meg de første 3 tegnene av ditt serienummer, en om gangen, for eksempel F B T.</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet heb begrepen. Kunt u me één voor één de eerste 3 tekens van uw serienummer doorgeven alstublieft, bijvoorbeeld F B T.</speak>',
	'pt':'<speak>Desculpe, não entendi. Por favor indique os primeiros 3 caracteres do seu número de série, um de cada vez, por exemplo, F B T.</speak>'
};

var prompts_2500_I_1 = 
{
	'en':'<speak>Please tell me the first 3 characters of your serial number one at a time, for example F B T.</speak>',
	'es':'<speak>Por favor, dígame los 3 primeros caracteres de su número de serie, uno a la vez, por ejemplo F B T.</speak>',
	'sv':'<speak>Vänligen meddela mig de första tre tecknen i ert serienummer, ett i taget, till exempel F B T.</speak>',
	'da':'<speak>Venglist indtal de förste 3 tegn af dit serienummer, en af gangen, for eksempel F B T.</speak>',
	'no':'<speak>Vennligst fortell meg de første 3 tegnene av ditt serienummer, en om gangen, for eksempel F B T.</speak>',
	'nl':'<speak>Kunt u me één voor één de eerste 3 tekens van uw serienummer doorgeven alstublieft, bijvoorbeeld F B T.</speak>',
	'pt':'<speak>Por favor indique os primeiros 3 caracteres do seu número de série, um de cada vez, por exemplo, F B T.</speak>'
};

var prompts_2500_confirm = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.  If that\'s correct say yes or press 1. Otherwise say no or press 2.</speak>',
	'es':'<speak>Obtuve  <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. Si eso es correcto, diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Det är <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. Om detta stämmer, säg ja eller tryck 1. Annars säg nej eller tryck 2.</speak>',
	'da':'<speak>Jeg modtog <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. Hvis dette er korrekt, sig ja eller tast 1. Ellers, sig nej eller tast 2.</speak>',
	'no':'<speak>Jeg fikk <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.  Hvis dette stemmer, si ja eller tast 1. Ellers si ja eller tast 2.</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>',
	'pt':'<speak>Indicou  <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. Se está correto diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};


var prompts_2550_I_1 = 
{
	'en':'<speak>Now, please say or enter the remaining digits of your serial number.</speak>',
	'es':'<speak>Ahora, por favor diga o escriba los restantes dígitos de su número de serie.</speak>',
	'sv':'<speak>Nu, vänligen säg, eller lägg in resten av siffrorna i ert serienummer.</speak>',
	'da':'<speak>Venligst indtal eller tast de resterende numre af dit serienummer</speak>',
	'no':'<speak>Nå, vennligst si eller tast inn de resterende tallene av ditt serienummer.</speak>',
	'nl':'<speak>Zeg of voer nu de resterende cijfers van uw serienummer in.</speak>',
	'pt':'<speak>Agora, por favor diga ou introduza os restantes dígitos do seu número de série.</speak>'
};

var prompts_2550_NM_1 = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Using your keypad please enter the remaining digits of your serial number.</speak>',
	'es':'<speak>Lo siento, no entendí eso. Usando su teclado, por favor escriba los restantes dígitos de su número de serie.</speak>',
	'sv':'<speak>Förlåt, det uppfattades inte. Vänligen använd knapparna på telefonen och lägg in resterande siffror i ert serienummer.</speak>',
	'da':'<speak>Beklager, det forstod jeg ikke. Venligst indtast de resterende numre af dit serienummer ved brug af det numeriske tastatur.</speak>',
	'no':'<speak>Jeg beklager, jeg forstod ikke dette. Ved å bruke nummertastene, vennligst legg inn de resterende tallene av ditt serienummer.</speak>',
	'nl':'<speak>Het spijt me, dat heb ik niet begrepen. Voer met behulp van uw toetsenbord de resterende cijfers van uw serienummer in.</speak>',
	'pt':'<speak>Desculpe, não entendi. Usando o seu teclado, por favor introduza os restantes dígitos do seu número de série.</speak>'
};

var prompts_2550_NI_1 = 
{
	'en':'<speak>Using your keypad please enter the remaining digits of your serial number.</speak>',
	'es':'<speak>Usando su teclado, por favor escriba los restantes dígitos de su número de serie.</speak>',
	'sv':'<speak>Genom att använda knapparna på er telefon, vänligen lägg in resterande siffror i ert serienummer.</speak>',
	'da':'<speak>ved brug af det numeriske tastatur, venligst indtast de resterende numre af dit serienummer.</speak>',
	'no':'<speak>Ved å bruke nummertastene, vennligst legg inn de resterende tallene av ditt serienummer.</speak>',
	'nl':'<speak>Voer met behulp van uw toetsenbord de resterende cijfers van uw serienummer in.</speak>',
	'pt':'<speak>Usando o seu teclado, por favor introduza os restantes dígitos do seu número de série.</speak>'
};

var prompts_950_I_1 = 
{
	'en':'<speak>Is your serial number a mixture of letters and numbers? Say yes or press 1. If not say no or press 2</speak>',
	'es':'<speak>¿Su número de serie es una mezcla de letras y números? Diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Om ert serienummer är en blandning av bokstäver och siffror, tryck 1. Om inte, tryck 2.</speak>',
	'da':'<speak>Er dit serienummer er en blandning af bogstaver og numre? Sig ja eller tast 1. Hvis ikke, sig nej eller tast 2.</speak>',
	'no':'<speak>Er ditt serienummer en blanding av bokstaver og tall? Si ja eller tast 1. Hvis ikke, si Nei eller tast 2.</speak>',
	'nl':'<speak>Is uw serienummer een combinatie van letters en cijfers? Zeg ja of druk op 1. Zo nee, zeg nee of druk op 2</speak>',
	'pt':'<speak>O seu número de série contém letras e números? Diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};

var prompts_950_NM_1 = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. If your serial number is a mixture of letters and numbers press 1. If not press 2.</speak>',
	'es':'<speak>Lo siento, no entendí eso. Si su número de serie es una mezcla de letras y números, púlse 1. Sino, púlse 2.</speak>',
	'sv':'<speak>Förlåt, det uppfattades inte. Vänligen använd knapparna på telefonen och lägg in resterande siffror i ert serienummer</speak>',
	'da':'<speak>Beklager, det forstod jeg ikke. Hvis dit serienummer er en blandning af bogstaver og numre, tast 1. hvis ikke, tast 2.</speak>',
	'no':'<speak>Jeg beklager, jeg forstod ikke dette. Hvis ditt serienummer består av en blanding av bokstaver og tall, trykk 1. Hvis ikke, trykk 2</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet begrepen. Als uw serienummer een combinatie van letters en cijfers is, drukt u op 1. Als dit niet het geval is, drukt u op 2.</speak>',
	'pt':'<speak>Desculpe, não entendi. Se o seu número de série contém letras e números prima 1. Caso contrário prima 2.</speak>'
};

var prompts_950_NI_1 = 
{
	'en':'<speak>If your serial number is a mixture of letters and numbers press 1. If not press 2.</speak>',
	'es':'<speak>Si su número de serie es una mezcla de letras y números, púlse 1. Sino, púlse 2.</speak>',
	'sv':'<speak>Om ert serienummer är en blandning av bokstäver och siffror, tryck 1. Om inte, tryck 2.</speak>',
	'da':'<speak>Hvis dit serienummer er en blandning af bogstaver og numre, tast 1. Hvis ikke, tast 2.</speak>',
	'no':'<speak>Hvis ditt serienummer er en blanding av bokstaver og tall, trykk 1. Hvis ikke, trykk 2.</speak>',
	'nl':'<speak>Als uw serienummer een combinatie van letters en cijfers is, drukt u op 1. Als dit niet het geval is, drukt u op 2.</speak>',
	'pt':'<speak>Se o seu número de série contém letras e números prima 1. Caso contrário prima 2.</speak>'
};

var prompts_2400_I_1 = 
{
	'en':'<speak>Please say or enter your serial number now.</speak>',
	'es':'<speak>Por favor, diga o escriba su número de serie ahora.</speak>',
	'sv':'<speak>Vänligen säg eller lägg in ert serienummer nu.</speak>',
	'da':'<speak>Venligst indtal eller tast dit serienummer nu.</speak>',
	'no':'<speak>Vennligst si eller tast inn ditt serienummer nå.</speak>',
	'nl':'<speak>Zeg of voer nu uw serienummer in.</speak>',
	'pt':'<speak>Diga ou introduza o seu número de série.</speak>'
};

var prompts_2400_NM_1 = 
{
	'en':'<speak>I\'m sorry I didn\'t get that. Using your keypad please enter  your serial number.</speak>',
	'es':'<speak>Lo siento, no entendí eso. Usando su teclado, escriba su número de serie.</speak>',
	'sv':'<speak>Förlåt, jag uppfattade inte riktigt. Om ert serienummer är en blandning av bokstäver och siffror, tryck 1. Om inte, tryck 2.</speak>',
	'da':'<speak>Beklager, det forstod jeg ikke. Indtast dit serienummer ved brug af det numeriske tastatur.</speak>',
	'no':'<speak>Jeg beklager, jeg forstod ikke dette. Ved å bruke nummertastene, vennligst legg inn ditt serienummer.</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet heb begrepen. Voer met behulp van uw toetsenbord uw serienummer in.</speak>',
	'pt':'<speak>Desculpe, não entendi. Usando o seu teclado, por favor introduza o seu número de série.</speak>'
};

var prompts_2400_NI_1 = 
{
	'en':'<speak>Using your keypad please enter  your serial number.</speak>',
	'es':'<speak>Usando su teclado, por favor escriba su número de serie.</speak>',
	'sv':'<speak>Genom att använda knapparna på telefonen, vänligen lägg in serienumret.</speak>',
	'da':'<speak>indtast dit serienummer ved brug af det numeriske tastatur.</speak>',
	'no':'<speak>Ved å bruke nummertastene, vennligst legg inn ditt serienummer.</speak>',
	'nl':'<speak>Voer met behulp van uw toetsenbord uw serienummer in.</speak>',
	'pt':'<speak>Usando o seu teclado, por favor introduza o seu número de série.</speak>'
};

var prompts_2400_confirm = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  If that\'s correct say yes or press 1. Otherwise say no or press 2.</speak>',
	'es':'<speak>Obtuve  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Si eso es correcto, diga sí o púlse 1. Sino, diga no o púlse 2.</speak>',
	'sv':'<speak>Det är <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Om detta stämmer, säg ja eller tryck 1. Annars säg nej eller tryck 2.</speak>',
	'da':'<speak>Jeg modtog <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Hvis dette er korrekt, sig ja eller tast 1. Ellers, sig nej eller tast 2.</speak>',
	'no':'<speak>Jeg fikk <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Hvis dette stemmer, si ja eller tast 1. Ellers si ja eller tast 2.</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>',
	'pt':'<speak>Indicou  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Se está correto diga SIM ou prima 1. Caso contrário diga NÃO ou prima 2.</speak>'
};

var remaining_serial_number_prompts =
    {
					'initial':prompts_2550_I_1,

					'retry': prompts_2550_NM_1
	};

	
var numeric_serial_number_prompts =
    {
					'initial':prompts_2650_I_1,

					'retry': prompts_2650_NM_1,
					'confirm':prompts_2650_confirm,

					'disconfirm': prompts_2650_I_1
	};

var retry_numeric_serial_number_prompts =
    {
					'initial':prompts_2650_NM_1,

					'retry': prompts_2650_NM_1,
					'confirm':prompts_2650_confirm,

					'disconfirm': prompts_2650_I_1
	};
	
var disconfirm_numeric_serial_number_prompts =
    {
					'initial':prompts_2650_disconfirm,

					'retry': prompts_2650_NM_1,
					'confirm':prompts_2650_confirm,

					'disconfirm': prompts_2650_I_1
	};
	
var serial_number_prompts =
    {
					'initial':prompts_2200_I_1,

					
					'confirm': prompts_2200_confirm,
					
	};
	
var first_three_alphas_prompts =
    {
					'initial':prompts_2500_I_1,

					'retry': prompts_2500_NM_1,
					'confirm':prompts_2500_confirm,

					'disconfirm': prompts_2500_I_1
	};
	
var contains_alphas_prompts =
    {
					'initial':prompts_950_I_1,

					'retry': prompts_950_NM_1,
					
	};

/***************** Prompts end *********************************************/




function createDialogConfig (fulfillment)  {
	console.log('**** BreakFix createDialogConfig called');
	
	df_fulfillment.addContainerIntentToConfig(configureWelcome());
	
	df_fulfillment.addSimpleIntentToConfig(configureGetSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureGetCompoundSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureContainsAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureNumericSerialNumber(fulfillment));
	df_fulfillment.addSimpleIntentToConfig(configureCompoundNumericSerialNumber());
	
	return df_fulfillment.addDefaultPropertiesToConfig(configureDefaults());
	
}

function configureDefaults () {
	return	{
				globals:[],
				max_retries:2,
				max_disconfirms:2,	
				max_retries_next:'Default Error Intent',
				max_disconfirms_next:'Default Error Intent'
			};
		
}




		
function configureWelcome () {
	console.log('*** creating BreakFixes welcome');
	return {
		name:"default_welcome_intent",			// this has to match intent 'display name' in UI
		
		prompt: " ",	
		
		next: getNextForWelcome
	}
}

function configureCompoundRemainingSerialNumber () {
	return	{
				intentName: 'compound_remaining_serial_number',
				paramName: 'remainingDigits', 
				confirm: false,
				postRecoFilter: parseRemainingSerialNumber,
				retryHandler: nomatchHandlerForRemainingDigits,
				prompts: remaining_serial_number_prompts,
				max_retries:2,
				next: getNextForCompoundRemainingSerialNumber,
				
			};
		
}

function configureGetSerialNumber () {
	return	{
				intentName: 'get_serial_number',
				paramName: 'serialNumber', 
				confirm: false,
				prompts: serial_number_prompts,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				max_retries:0,
				max_disconfirms:0,
				next: 'end_conversation',
				max_retries_next: 'Default Error Intent',
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function getNextForSerialNumber(fulfillment) {
	df_fulfillment.setGlobalParameter(fulfillment,  'num_retries', 0);
	df_fulfillment.setGlobalParameter(fulfillment,  'num_disconfirms', 0);
	df_fulfillment.setGlobalParameter(fulfillment,  'intent_name', 'first_three_alphas');
	return 'first_three_alphas';
}

function getNextForRetries(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (lang == 'es-es') {
		console.log(' *** in getNextForRetries; fulfillment = ' + JSON.stringify(fulfillment));
		var num_retries = df_fulfillment.getGlobalParameter(fulfillment,  'num_retries');
		var intent_name = df_fulfillment.getGlobalParameter(fulfillment,  'intent_name');
		console.log(' *** in getNextForRetries; num_retries = ' + num_retries + '; intent_name = ' + intent_name);
		if (num_retries == 0) {
			df_fulfillment.setGlobalParameter(fulfillment,  'num_retries', 1);
			console.log(' *** in getNextForRetries; num_retries = 0; returning to the intent');
			return  intent_name;
		}
		console.log(' *** in getNextForRetries; num_retries > 0; returning to the Default Error Intent');
		return 'Default Error Intent';
	}
	return 'Default Error Intent';
}

function configureGetCompoundSerialNumber () {
	return	{
				intentName: 'get_compound_serial_number',
				paramName: 'serialNumber', 
				confirm: true,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				prompts: serial_number_prompts,
				max_retries:0,
				max_disconfirms:1,
				next: getNextForCompoundSerialNumber,
				max_retries_next: getNextIntentForMaxErrors,
				max_disconfirms_next: getNextIntentForMaxErrors,
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function configureContainsAlphas () {
	return	{
				intentName: 'contains_alphas',
				paramName: 'yesNo', 
				confirm: false,
				prompts: contains_alphas_prompts,
				max_retries:2,
				max_retries_next: 'numeric_serial_number',
				next: getNextIntentForContainsAlphas,
				// this is a function that will be evaluated at runtime to figure out where to go next;
				// if the caller said 'yes', the call will go to 'first_three_alphas' intent;
				// if the caller said 'no', it will go back to 'get_serial_number' for another attempt
				
			};
		
}

function configureFirstThreeAlphas () {
	return	{
				intentName: 'first_three_alphas',
				paramName: 'firstThree', 
				confirm: true,
				//postRecoFilter: parseFirstThree,
				//retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				next: 'remaining_serial_number',
				
			};
		
}

function configureCompoundFirstThreeAlphas () {
	return	{
				intentName: 'compound_first_three_alphas',
				paramName: 'firstThree', 
				confirm: false,
				postRecoFilter: parseFirstThree,
				retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				next: getNextForCompoundFirstThreeAlphas,
				
			};
		
}

function configureRemainingSerialNumber () {
	return	{
				intentName: 'remaining_serial_number',
				paramName: 'remainingDigits', 
				// postRecoFilter: parseRemainingSerialNumber,
				// retryHandler: nomatchHandlerForRemainingDigits,
				confirm: false,
				prompts: remaining_serial_number_prompts,
				max_retries:2,
				next: 'end_conversation',
				
			};
		
}

function configureNumericSerialNumber (fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	const num_retries = df_fulfillment.getGlobalParameter(fulfillment, 'num_retries');
	const num_disconfirms = df_fulfillment.getGlobalParameter(fulfillment, 'num_disconfirms');
	var prompts = numeric_serial_number_prompts;
	if (lang == 'es-es') {
		if (num_retries == 1) { 
			prompts = retry_numeric_serial_number_prompts;
		} else if (num_disconfirms == 1) {
			prompts = disconfirm_numeric_serial_number_prompts;
		}
	return 	{
				intentName: 'numeric_serial_number',
				paramName: 'serialNumber', 
				// postRecoFilter: parseNumericSerialNumber,
				// retryHandler: nomatchHandlerForNumericSerialNumber,
				confirm: false,
				prompts: prompts,
				max_retries:0,
				next: 'end_conversation',
				max_retries_next: getNextForRetries
				
			};
		
	} 
	return	{
				intentName: 'numeric_serial_number',
				paramName: 'serialNumber', 
				// postRecoFilter: parseNumericSerialNumber,
				// retryHandler: nomatchHandlerForNumericSerialNumber,
				confirm: false,
				prompts: numeric_serial_number_prompts,
				max_retries:0,
				next: 'end_conversation',
				max_retries_next: getNextForRetries
				
			};
		
}


function configureCompoundNumericSerialNumber () {
	return	{
				intentName: 'compound_numeric_serial_number',
				paramName: 'serialNumber', 
				postRecoFilter: parseCompoundNumericSerialNumber,
				retryHandler: nomatchHandlerForCompoundNumericSerialNumber,
				confirm: true,
				prompts: numeric_serial_number_prompts,
				max_retries:2,
				max_disconfirms: 2,
				next: setSerialNumberParamAndReturnEndConversation,
				max_retries_next: getMaxErrorsNextForNumericSerialNumber,
				max_disconfirms_next: getMaxErrorsNextForNumericSerialNumber
				
			};
		
}


function getSerialNumberIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'get_serial_number';
	}
	return 'get_compound_serial_number';
}

function getNumericSerialNumberIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsDigitStrings(lang)) {
		return 'numeric_serial_number';
	}
	return 'compound_numeric_serial_number';
}

function getRemainingSerialNumberIntent(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsDigitStrings(lang)) {
		return 'remaining_serial_number';
	}
	return 'compound_remaining_serial_number';
}

function getFirstThreeAlphasIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'first_three_alphas';
	}
	return 'compound_first_three_alphas';
}

function getNextForWelcome(fulfillment) {
	console.log('*** in getNextForWelcome;');
	var welcome = fulfillment.getContext('welcome');
	var skipNewTS = false;
	var lang = '';
	var usa3menu = false;
	var intent_name = '';
	var num_retries = 0;
	var num_disocnfirms = 0;
	if (welcome && welcome.parameters) {
		console.log('*** in getNextForWelcome; welcome context is: ' + JSON.stringify(welcome));
		lang = welcome.parameters.LanguageCode || '';
		skipNewTS = welcome.parameters.skipNewTS || false;
		usa3menu = welcome.parameters.A3Menu || false;
		intent_name = welcome.parameters.intent_name || '';
		
	}
	
	if (lang == 'es-es') {
		if (intent_name) {
			return intent_name;
		}
		return "BaseMenu";
	}
	
	if (lang == 'en-us') {
		df_fulfillment.setLanguage(fulfillment, 'en');
		console.log('*** in getNextForWelcome; lang is en-US');
		if ((usa3menu == false) && (skipNewTS == false)) {
			console.log('*** in getNextForWelcome; both usa3menu and skipNewTS are false -- returning get_serial_number');
			return 'newTechSupport';
		} else if (skipNewTS != false) {
			return 'first_three_alphas';
		} else if (usa3menu != false) {
			return 'USA3Menu';
		}
	} else {
		return 'BaseMenu';
	}
}
	
function languageSupportsAlphaNumerics(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('pt-pt'  == locale) || ('sv-se'  == locale) || ('nl-nl'  == locale) || ('da-dk'  == locale));
}

function languageSupportsDigitStrings(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('da-dk'  == locale));
}





function nomatchHandler(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandler; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",serialNumber:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}


function nomatchHandlerForRemainingDigits(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForRemainingDigits; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",remainingDigits:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForCompoundNumericSerialNumber(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForCompoundNumericSerialNumber; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nnomatchHandlerForCompoundNumericSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an numeric string!
	    
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",serialNumber:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForFirstThreeAlphas(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForFirstThreeAlphas; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandlerForFirstThreeAlphas; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForFirstThreeAlphas; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandlerForFirstThreeAlphas; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for 3 alphas
	
	if(regex.test(text)) { // it was an alpha-numeric string!
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",firstThree:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForFirstThreeAlphas; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForFirstThreeAlphas; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}




function parseSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseSerialNumber; queryText is: ' + text);
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.serialNumber = text;
	} else {
		console.log('*** in parseSerialNumber; cleaned up text did not match the RegEx; rejecting');
		filledParams.serialNumber = undefined;
	}
	
	/**
	const serialNumObj = filledParams.serialNumber;
		
	var serialNumString = "";
	if (serialNumObj['d']) {
		serialNumString = serialNumObj['d'];
	} else {
		for (var i = 1; i < 16; i++) {
			const a = 'a' + i;
			if (serialNumObj[a]) {
				serialNumString = serialNumString + serialNumObj[a];
			}
		}
	}
	filledParams.serialNumber = serialNumString;
	*/
	console.log('*** in parseSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseRemainingSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseRemainingSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseRemainingSerialNumber; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseRemainingSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){3,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseRemainingSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.remainingDigits = text;
	} else {
		console.log('*** in parseRemainingSerialNumber; cleaned up text did not matched the RegEx; rejecting');
		filledParams.remainingDigits = undefined;
	}
	
	console.log('*** in parseRemainingSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseCompoundNumericSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseCompoundNumericSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseCompoundNumericSerialNumber; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseCompoundNumericSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseCompoundNumericSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.serialNumber = text;
	} else {
		console.log('*** in parseCompoundNumericSerialNumber; cleaned up text did not matched the RegEx; rejecting');
		filledParams.serialNumber = undefined;
	}
	
	console.log('*** in parseCompoundNumericSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

/**
 Possibly not needed anymore
*/
function replaceSpelledDigits(lang, text) {
	
	if (lang == 'en-gb') {
		var regex = new RegExp('one','g');
		text = text.replace(regex, '1');
		regex = new RegExp('two','g');
		text = text.replace(regex, '2');
		regex = new RegExp('three','g');
		text = text.replace(regex, '3');
		regex = new RegExp('four','g');
		text = text.replace(regex, '4');
		regex = new RegExp('five','g');
		text = text.replace(regex, '5');
		regex = new RegExp('six','g');
		text = text.replace(regex, '6');
		regex = new RegExp('seven','g');
		text = text.replace(regex, '7');
		regex = new RegExp('eight','g');
		text = text.replace(regex, '8');
		regex = new RegExp('nine','g');
		text = text.replace(regex, '9');
		regex = new RegExp('zero','g');
		text = text.replace(regex, '0');
		regex = new RegExp('oh','g');
		text = text.replace(regex, '0');
		regex = new RegExp('for','g');
		text = text.replace(regex, '4');
		regex = new RegExp('to','g');
		text = text.replace(regex, '2');
		return text;
	}
	return text;
}

function parseFirstThree(fulfillment, filledParams) {
	console.log('*** in parseFirstThree; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseFirstThree; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseFirstThree; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseFirstThree; cleaned up text matched the RegEx; accepting');
		filledParams.firstThree = text;
	} else {
		console.log('*** in parseFirstThree; cleaned up text did not matched the RegEx; rejecting');
		filledParams.firstThree = undefined;
	}
	
	
	console.log('*** in parseFirstThree; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}



function getNextForCompoundSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'get_compound_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundRemainingSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_remaining_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'remaining_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundFirstThreeAlphas(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_first_three_alphas');
	console.log('**** in getNextForCompoundFirstThreeAlphas: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'first_three_alphas', params);
	}
	return getRemainingSerialNumberIntent(fulfillment);
}


/**
  Returns the name of the intent to transition to when the current intent ('get_serial_number')
  errors out.
  
  Per call flow logic, allow 1 combined error -- i.e. send the caller to 'contains_alpha' 
  to try to gather serial number by parts; on a third error, go to 'Default Error Intent'
*/
function getNextIntentForMaxErrors(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_serial_number_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number_errors', numErrors + 1);
	var lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (lang == 'en-us') {
		return 'first_three_alphas';
	}
	return 'contains_alphas';
}




function getNextIntentForContainsAlphas(fulfillment) {
	const response = df_fulfillment.getFilledRequiredParameter(fulfillment, 'yesNo', 'contains_alphas');
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode') || 'es-es';
	// get the value of 'yesNo' parameter filled in 'contains_alphas' intent
	if (response == 'yes') {
		if (lang != 'es-es') {
			return 'end_conversation';
		} else {
			return 'get_serial_number';
		}
		
	}
	return getNumericSerialNumberIntentName(fulfillment);
}



function setSerialNumberParamAndReturnEndConversation (fulfillment) {
	const serialNumber = df_fulfillment.getFilledRequiredParameter(fulfillment, 'serialNumber', 'compound_numeric_serial_number');
	console.log('*** serialNumber collected in compound_numeric_serial_number: ' + serialNumber);
	var getSerialNumberParams = {'serialNumber': serialNumber};
	df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number', getSerialNumberParams);
	console.log('*** fulfillment after setting collected compound_numeric_serial_number: ' + JSON.fulfillment);
	return 'end_conversation';
}

function getMaxErrorsNextForNumericSerialNumber(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode') || 'es-es';
	if ((lang != 'es-es') && (lang != 'en-us')) {
		return 'end_conversation';
	}
	
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_numeric_serial_number_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_numeric_serial_number_errors', numErrors + 1);
	return 'Default Error Intent';
}

function getMaxErrorsNextForFirstThreeAlphas(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode') || 'es-es';
	if ((lang != 'es-es') && (lang != 'en-us')) {
		return 'end_conversation';
	}
	
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_first_three_alphas_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_first_three_alphas_errors', numErrors + 1);
	return 'Default Error Intent';
}

function getMaxErrorsNextForRemainingDigits(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode') || 'es-es';
	if (lang != 'es-es' && lang != 'en-us') {
		return 'end_conversation';
	}
	
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_remaining_digits_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_remaining_digits_errors', numErrors + 1);
	return 'Default Error Intent';
}







exports.createDialogConfig = createDialogConfig;

