	'use strict';

const functions = require('firebase-functions');
const dialogflowFulfillment = require('./dialogflow-fulfillment-builder');


const df_fulfillment = require('./state-machine/fulfillment');



const config = {
    'platformsEnabled': ['TEXT'] // define the platforms to be supported as per requirement
};


/********* Prompts start **************************/

var contains_alphas_initial_prompts = 
{
	'en':'<speak>Is your serial number a mixture of letters and numbers? Say yes or press 1. If not say no or press 2.</speak>',
	'de':'<speak>Besteht Ihre Seriennummer aus Buchstaben und Zahlen? Sagen Sie  "Ja" oder bitte drücken Sie eins. Andernfalls sagen Sie "Nein" oder drücken Sie 2.</speak>',
	'fr':'<speak>Votre numéro de série est-il un mélange de lettres et de chiffres? Dites oui ou appuyez sur 1. Sinon, dites non ou appuyez sur 2.</speak>',
	'it':'<speak>Il suo numero di serie è composto da lettere e numeri? Dica Sì o prema 1. oppure dica No o prema 2. </speak>',
	'nl':'<speak>Is uw serienummer een combinatie van letters en cijfers? Zeg ja of druk op 1. Zo nee, zeg nee of druk op 2. </speak>'
};

var contains_alphas_nomatch_prompts = 
{
	'en':'<speak>I\'m sorry I didn\'t get that. If your serial number is a mixture of letters and numbers press 1. If not press 2.</speak>',
	'de':'<speak>Entschuldigung, ich habe das leider nicht verstanden. Falls Ihre Seriennummer aus Buchstaben und Zahlen besteht drücken Sie bitte eins. Falls nicht drücken Sie bitte 2.</speak>',
	'fr':'<speak>Je suis désole, je n\'ai pas compris. Si votre numéro de série est un mélange de lettres et de chiffres, appuyez sur 1. Sinon, appuyez sur 2.</speak>',
	'it':'<speak>Mi scusi non ho compreso. Se il suo numero di serie è composto da lettere e numeri prema 1 altrimenti prema 2</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet begrepen. Als uw serienummer een combinatie van letters en cijfers is, drukt u op 1. Als dit niet het geval is, drukt u op 2.</speak>'
};

var contains_alphas_noinput_prompts = 
{
	'en':'<speak>If your serial number is a mixture of letters and numbers press 1. If not press 2.</speak>',
	'de':'<speak>Falls Ihre Seriennummer aus Buchstaben und Zahlen besteht drücken Sie bitte eins. Falls nicht drücken Sie bitte 2.</speak>',
	'fr':'<speak>Si votre numéro de série est un mélange de lettres et de chiffres, appuyez sur 1. Sinon, appuyez sur 2</speak>',
	'it':'<speak>Se il suo numero di serie è composto da lettere e numeri prema 1 altrimenti prema 2</speak>',
	'nl':'<speak>Als uw serienummer een combinatie van letters en cijfers is, drukt u op 1. Als dit niet het geval is, drukt u op 2.</speak>'
};

var numeric_serial_number_initial_prompts = 
{
	'en':'<speak>Please say or enter your serial number now.</speak>',
	'de':'<speak>Nun sagen Sie oder geben Sie bitte Ihre Serriennummer ein.</speak>',
	'fr':'<speak>OK, veuillez me donner le numéro de série de votre machine</speak>',
	'it':'<speak>Per favore dica o inserisca il suo numero di serie</speak>',
	'nl':'<speak>Zeg of voer nu uw serienummer in.</speak>'
};

var numeric_serial_number_nomatch_prompts = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Using your keypad please enter  your serial number.</speak>',
	'de':'<speak>Entschuldigung, ich habe das leider nicht verstanden. Bitte geben Sie Ihre Seriennummer ein indem Sie Ihre Telefontastatur benutzen.</speak>',
	'fr':'<speak>Je suis désolé, je n\'ai pas compris. Veuillez me donner le numéro de série de votre machine.</speak>',
	'it':'<speak>Mi scusi non ho compreso. Per favore digiti sulla tastiera il suo numero di serie. </speak>',
	'nl':'<speak>Het spijt me dat ik dat niet begrepen. Zeg of voer nu uw serienummer in.</speak>'
};

var numeric_serial_number_noinput_prompts = 
{
	'en':'<speak>Using your keypad please enter your serial number.</speak>',
	'de':'<speak>Bitte benutzen Sie die Telefontastatur um Ihre Seriennummer einzugeben.</speak>',
	'fr':'<speak>À l\'aide de votre clavier, veuillez me donner le numéro de série de votre machine.</speak>',
	'it':'<speak>Per favore digiti sulla tastiera il suo numero di serie.</speak>',
	'nl':'<speak>Zeg of voer nu uw serienummer in.</speak>'
};

var numeric_serial_number_confirm_prompts = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Is that correct? say yes or press 1. Say no or press 2.</speak>',
	'de':'<speak>Ich habe  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Ist diese Eingabe richtig? Sagen Sie "Ja" oder drücken Sie eins. Sagen Sie "Nein" oder drücken Sie 2.</speak>',
	'fr':'<speak>J\'ai eu <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.   Est-ce exact? Dites oui ou appuyez sur 1. Dites non ou appuyez sur 2.</speak>',
	'it':'<speak>Il numero digitato è <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. È corretto? Dica sì o prema 1. Dica no o prema 2.</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>'
};

var serial_number_initial_prompts = 
{
	'en':'<speak>In order to assist you I need to get the serial number of your machine. Please tell me that number now.</speak>',
	'de':'<speak>Damit ich Ihnen helfen kann, benötige ich die Seriennummer Ihres Gerätes. Bitte geben Sie mir jetzt die Nummer.</speak>',
	'fr':'<speak>OK, veuillez me donner le numéro de série de votre machine. </speak>',
	'it':'<speak>Per poterla assistere abbiamo bisogno del numero di serie del suo dispositivo. Per favore dica il numero adesso</speak>',
	'nl':'<speak>Om u te helpen, heb ik het serienummer van uw machine nodig. Kunt u dat aan mij doorgeven?</speak>'
};

var serial_number_confirm_prompts = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. Is that correct? say yes or press 1. Say no or press 2.</speak>',
	'de':'<speak>Ich habe  <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Ist diese Eingabe richtig? Sagen Sie "Ja" oder drücken Sie eins. Sagen Sie "Nein" oder drücken Sie 2.</speak>',
	'fr':'<speak>J\'ai eu <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.   Est-ce exact? Dites oui ou appuyez sur 1. Dites non ou appuyez sur 2.</speak>',
	'it':'<speak>Il numero digitato è <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>. È corretto? Dica sì o prema 1. Dica no o prema 2</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>'
};


var first_three_alphas_initial_prompts = 
{
	'en':'<speak>Please tell me the first 3 characters of your serial number one at a time, for example F B T.</speak>',
	'de':'<speak>Bitte sagen Sie mir nacheinander die ersten 3 Buchstaben Ihrer Seriennummer, zum Bespiel F B T.</speak>',
	'fr':'<speak>Veuillez me dire les 3 premiers caractères de votre numéro de série, par exemple f b t.</speak>',
	'it':'<speak>Per favore dica le prime 3 cifre del suo numero di serie una alla volta, per esempio F B T</speak>',
	'nl':'<speak>Kunt u me één voor één de eerste 3 tekens van uw serienummer doorgeven alstublieft, bijvoorbeeld F B T.</speak>'
};

var first_three_alphas_nomatch_prompts = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Please tell me the first 3 characters of your serial number one at a time, for example F B T.</speak>',
	'de':'<speak>Entschuldigung, ich habe das leider nicht verstanden. Bitte sagen Sie mir nacheinander die ersten 3 Buchstaben Ihrer Seriennummer, zum Bespiel F B T.</speak>',
	'fr':'<speak>Je suis désolé, je n\'ai pas compris. Veuillez me dire les 3 premiers caractères de votre numéro de série, par exemple F B T. </speak>',
	'it':'<speak>Mi dispiace non ho compreso. Per favore dica le prime 3 cifre del suo numero di serie una alla volta, per esempio F B T</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet begrepen. Kunt u me één voor één de eerste 3 tekens van uw serienummer doorgeven alstublieft, bijvoorbeeld F B T.</speak>'
};

var first_three_alphas_noinput_prompts = 
{
	'en':'<speak>Please tell me the first 3 characters of your serial number one at a time, for example F B T.</speak>',
	'de':'<speak>Bitte sagen Sie mir nacheinander die ersten 3 Buchstaben Ihrer Seriennummer, zum Bespiel F B T.</speak>',
	'fr':'<speak>Veuillez me dire les 3 premiers caractères de votre numéro de série, par exemple F B T.</speak>',
	'it':'<speak>Per favore dica le prime 3 cifre del suo numero di serie una alla volta, per esempio F B T</speak>',
	'nl':'<speak>Kunt u me één voor één de eerste 3 tekens van uw serienummer doorgeven alstublieft, bijvoorbeeld F B T.</speak>'
};

var first_three_alphas_confirm_prompts = 
{
	'en':'<speak>I got <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. Is that correct? say yes or press 1. Say no or press 2.</speak>',
	'de':'<speak>Ich habe  <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.  Ist diese Eingabe richtig? Sagen Sie "Ja" oder drücken Sie eins. Sagen Sie "Nein" oder drücken Sie 2.</speak>',
	'fr':'<speak>J\'ai eu <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.   Est-ce exact? Dites oui ou appuyez sur 1. Dites non ou appuyez sur 2.</speak>',
	'it':'<speak>Il numero digitato è <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>. È corretto? Dica sì o prema 1. Dica no o prema 2</speak>',
	'nl':'<speak>Ik heb <prosody rate="slow"> <say-as interpret-as="characters"> ${firstThree} </say-as> </prosody>.  Als dat juist is, zeg ja of druk op 1. Zeg anders nee of druk op 2.</speak>'
};


var remaining_serial_number_initial_prompts = 
{
	'en':'<speak>Now, please say or enter the remaining digits of your serial number.</speak>',
	'de':'<speak>Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.</speak>',
	'fr':'<speak>Maintenant, dites ou entrez les chiffres restants de votre numéro de série.</speak>',
	'it':'<speak>Adesso per favore dica o inserisca le cifre rimanenti del suo numero di serie</speak>',
	'nl':'<speak>Zeg of voer nu de resterende cijfers van uw serienummer in.</speak>'
};


var remaining_serial_number_noinput_prompts = 
{
	'en':'<speak>Now, please say or enter the remaining digits of your serial number.</speak>',
	'de':'<speak>Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.</speak>',
	'fr':'<speak>Maintenant, dites ou entrez les chiffres restants de votre numéro de série.</speak>',
	'it':'<speak>Adesso per favore dica o inserisca le cifre rimanenti del suo numero di serie</speak>',
	'nl':'<speak>Zeg of voer nu de resterende cijfers van uw serienummer in.</speak>'
};

var remaining_serial_number_nomatch_prompts = 
{
	'en':'<speak>I\'m sorry, I didn\'t get that. Using your keypad please enter the remaining digits of your serial number.</speak>',
	'de':'<speak>Entschuldigung, ich habe das leider nicht verstanden. Bitte benutzen Sie Ihre Telefontastatur um die restlichen Zahlen ihrer Seriennummer einzugeben.</speak>',
	'fr':'<speak>Je suis désolé, je n\'ai pas compris. A l\'aide de  votre clavier s\'il vous plaît entrer les chiffres restants de votre numéro de série. </speak>',
	'it':'<speak>Mi dispiace non ho compreso. Per favore digiti sulla tastiera le cifre rimanenti del suo numero di serie</speak>',
	'nl':'<speak>Het spijt me dat ik dat niet begrepen. Zeg of voer nu de resterende cijfers van uw serienummer in.</speak>'
};



var remaining_serial_number_prompts =
    {
					'initial': remaining_serial_number_initial_prompts,

					'retry': remaining_serial_number_nomatch_prompts
	};
	
var numeric_serial_number_prompts =
    {
					'initial': numeric_serial_number_initial_prompts,

					'retry': numeric_serial_number_nomatch_prompts,
					'confirm':numeric_serial_number_confirm_prompts,

					'disconfirm': numeric_serial_number_initial_prompts
	};
	
var serial_number_prompts =
    {
					'initial': serial_number_initial_prompts,

					
					'confirm': serial_number_confirm_prompts
					
	};
	
var first_three_alphas_prompts =
    {
					'initial': first_three_alphas_initial_prompts,

					'retry': first_three_alphas_nomatch_prompts,
					'confirm': first_three_alphas_confirm_prompts,

					'disconfirm': first_three_alphas_initial_prompts
	};
	
var contains_alphas_prompts =
    {
					'initial': contains_alphas_initial_prompts,

					'retry': contains_alphas_nomatch_prompts,
					
	};

/***************** Prompts end *********************************************/



function createDialogConfig (fulfillment)  {
	console.log('**** XPS createDialogConfig called');
	
	df_fulfillment.addContainerIntentToConfig(configureWelcome());
	
	df_fulfillment.addSimpleIntentToConfig(configureGetSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureGetCompoundSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureContainsAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureNumericSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundNumericSerialNumber());
	
	return df_fulfillment.addDefaultPropertiesToConfig(configureDefaults());
	
}

function configureDefaults () {
	return	{
				globals:[],
				max_retries:2,
				max_disconfirms:2,	
				max_retries_next:'Default Error Intent',
				max_disconfirms_next:'Default Error Intent'
			};
		
}




		
function configureWelcome () {
	return {
		name:"default_welcome_intent",			// this has to match intent 'display name' in UI
		
		prompt: " ",	
		
		next: getNextForWelcome
	}
}

function configureCompoundRemainingSerialNumber () {
	return	{
				intentName: 'compound_remaining_serial_number',
				paramName: 'remainingDigits', 
				confirm: false,
				postRecoFilter: parseRemainingSerialNumber,
				retryHandler: nomatchHandlerForRemainingDigits,
				prompts: remaining_serial_number_prompts,
				max_retries:2,
				next: getNextForCompoundRemainingSerialNumber,
				
			};
		
}

function configureGetSerialNumber () {
	return	{
				intentName: 'get_serial_number',
				paramName: 'serialNumber', 
				confirm: true,
				prompts: serial_number_prompts,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				max_retries:0,
				max_disconfirms:1,
				next: 'end_conversation',
				max_retries_next: 'first_three_alphas',
				max_disconfirms_next: 'first_three_alphas',
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function configureGetCompoundSerialNumber () {
	return	{
				intentName: 'get_compound_serial_number',
				paramName: 'serialNumber', 
				confirm: true,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				prompts: serial_number_prompts,
				max_retries:0,
				max_disconfirms:1,
				next: getNextForCompoundSerialNumber,
				max_retries_next: getNextIntentForMaxErrors,
				max_disconfirms_next: getNextIntentForMaxErrors,
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function configureContainsAlphas () {
	return	{
				intentName: 'contains_alphas',
				paramName: 'yesNo', 
				confirm: false,
				prompts: contains_alphas_prompts,
				max_retries:2,
				max_retries_next: getMaxErrorsNextForContainsAlphas,
				next: getNextIntentForContainsAlphas,
				// this is a function that will be evaluated at runtime to figure out where to go next;
				// if the caller said 'yes', the call will go to 'first_three_alphas' intent;
				// if the caller said 'no', it will go back to 'get_serial_number' for another attempt
				
			};
		
}

function configureFirstThreeAlphas () {
	return	{
				intentName: 'first_three_alphas',
				paramName: 'firstThree', 
				confirm: true,
				//postRecoFilter: parseFirstThree,
				//retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				max_disconfirms:2,
				next: 'remaining_serial_number',
				max_retries_next:getMaxErrorsNextForFirstThreeAlphas,
				max_disconfirms_next:getMaxErrorsNextForFirstThreeAlphas,
			};
		
}

function configureCompoundFirstThreeAlphas () {
	return	{
				intentName: 'compound_first_three_alphas',
				paramName: 'firstThree', 
				confirm: false,
				postRecoFilter: parseFirstThree,
				retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				next: getNextForCompoundFirstThreeAlphas,
				
			};
		
}

function configureRemainingSerialNumber () {
	return	{
				intentName: 'remaining_serial_number',
				paramName: 'remainingDigits', 
				// postRecoFilter: parseRemainingSerialNumber,
				// retryHandler: nomatchHandlerForRemainingDigits,
				confirm: false,
				prompts: remaining_serial_number_prompts,
				max_retries:2,
				max_retries_next:getMaxErrorsNextForRemainingDigits,
				max_disconfirms_next:getMaxErrorsNextForRemainingDigits,
				next: 'end_conversation',
				
			};
		
}

function configureNumericSerialNumber () {
	return	{
				intentName: 'numeric_serial_number',
				paramName: 'serialNumber', 
				// postRecoFilter: parseNumericSerialNumber,
				// retryHandler: nomatchHandlerForNumericSerialNumber,
				confirm: true,
				prompts: numeric_serial_number_prompts,
				max_retries:2,
				max_disconfirms: 2,
				next: 'end_conversation',
				max_retries_next: getMaxErrorsNextForNumericSerialNumber,
				max_disconfirms_next: getMaxErrorsNextForNumericSerialNumber
				
			};
		
}


function configureCompoundNumericSerialNumber () {
	return	{
				intentName: 'compound_numeric_serial_number',
				paramName: 'serialNumber', 
				postRecoFilter: parseCompoundNumericSerialNumber,
				retryHandler: nomatchHandlerForCompoundNumericSerialNumber,
				confirm: true,
				prompts: numeric_serial_number_prompts,
				max_retries:2,
				max_disconfirms: 2,
				next: setSerialNumberParamAndReturnEndConversation,
				max_retries_next: getMaxErrorsNextForNumericSerialNumber,
				max_disconfirms_next: getMaxErrorsNextForNumericSerialNumber
				
			};
		
}


function getSerialNumberIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'get_serial_number';
	}
	return 'get_compound_serial_number';
}

function getNumericSerialNumberIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsDigitStrings(lang)) {
		return 'numeric_serial_number';
	}
	return 'compound_numeric_serial_number';
}

function getRemainingSerialNumberIntent(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsDigitStrings(lang)) {
		return 'remaining_serial_number';
	}
	return 'compound_remaining_serial_number';
}

function getFirstThreeAlphasIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'first_three_alphas';
	}
	return 'compound_first_three_alphas';
}

function getNextForWelcome(fulfillment) {
	console.log('*** in getNextForWelcome;');
	var welcome = fulfillment.getContext('welcome');
	var skipNewTS = false;
	var lang = '';
	var usa3menu = false;
	var xps = false;
	if (welcome && welcome.parameters) {
		console.log('*** in getNextForWelcome; welcome context is: ' + JSON.stringify(welcome));
		lang = welcome.parameters.LanguageCode || '';
		skipNewTS = welcome.parameters.skipNewTS || false;
		usa3menu = welcome.parameters.A3Menu || false;
		xps = welcome.parameters.XPS || welcome.parameters.xps || false;
	}
	
	if (lang == 'en-us') {
		df_fulfillment.setLanguage(fulfillment, 'en');
		console.log('*** in getNextForWelcome; lang is en-US');
		if ((usa3menu == false) && (skipNewTS == false)) {
			console.log('*** in getNextForWelcome; both usa3menu and skipNewTS are false -- returning get_serial_number');
			return 'newTechSupport';
		} else if (skipNewTS != false) {
			return 'get_serial_number';
		} else if (usa3menu != false) {
			return 'USA3Menu';
		}
	} else {
		if (xps) {
			return 'BaseMenuXPS';
		}
		return 'BaseMenu';
	}
}
	
function languageSupportsAlphaNumerics(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('pt-pt'  == locale) || ('sv-se'  == locale) || ('nl-nl'  == locale) || ('da-dk'  == locale));
}

function languageSupportsDigitStrings(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('da-dk'  == locale));
}

function languageBelgiumDutchDigitStrings(lang) {
	const locale = lang.toLowerCase();
	return ('nl-be' == locale);
}




function nomatchHandler(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandler; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",serialNumber:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}


function nomatchHandlerForRemainingDigits(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForRemainingDigits; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",remainingDigits:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForCompoundNumericSerialNumber(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForCompoundNumericSerialNumber; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nnomatchHandlerForCompoundNumericSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an numeric string!
	    
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",serialNumber:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForCompoundNumericSerialNumber; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForFirstThreeAlphas(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForFirstThreeAlphas; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandlerForFirstThreeAlphas; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForFirstThreeAlphas; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandlerForFirstThreeAlphas; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for 3 alphas
	
	if(regex.test(text)) { // it was an alpha-numeric string!
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",firstThree:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForFirstThreeAlphas; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForFirstThreeAlphas; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}




function parseSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseSerialNumber; queryText is: ' + text);
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.serialNumber = text;
	} else {
		console.log('*** in parseSerialNumber; cleaned up text did not match the RegEx; rejecting');
		filledParams.serialNumber = undefined;
	}
	
	/**
	const serialNumObj = filledParams.serialNumber;
		
	var serialNumString = "";
	if (serialNumObj['d']) {
		serialNumString = serialNumObj['d'];
	} else {
		for (var i = 1; i < 16; i++) {
			const a = 'a' + i;
			if (serialNumObj[a]) {
				serialNumString = serialNumString + serialNumObj[a];
			}
		}
	}
	filledParams.serialNumber = serialNumString;
	*/
	console.log('*** in parseSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseRemainingSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseRemainingSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseRemainingSerialNumber; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseRemainingSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){3,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseRemainingSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.remainingDigits = text;
	} else {
		console.log('*** in parseRemainingSerialNumber; cleaned up text did not matched the RegEx; rejecting');
		filledParams.remainingDigits = undefined;
	}
	
	console.log('*** in parseRemainingSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseCompoundNumericSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseCompoundNumericSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseCompoundNumericSerialNumber; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseCompoundNumericSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseCompoundNumericSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.serialNumber = text;
	} else {
		console.log('*** in parseCompoundNumericSerialNumber; cleaned up text did not matched the RegEx; rejecting');
		filledParams.serialNumber = undefined;
	}
	
	console.log('*** in parseCompoundNumericSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

/**
 Possibly not needed anymore
*/
function replaceSpelledDigits(lang, text) {
	
	if (lang == 'en-gb') {
		var regex = new RegExp('one','g');
		text = text.replace(regex, '1');
		regex = new RegExp('two','g');
		text = text.replace(regex, '2');
		regex = new RegExp('three','g');
		text = text.replace(regex, '3');
		regex = new RegExp('four','g');
		text = text.replace(regex, '4');
		regex = new RegExp('five','g');
		text = text.replace(regex, '5');
		regex = new RegExp('six','g');
		text = text.replace(regex, '6');
		regex = new RegExp('seven','g');
		text = text.replace(regex, '7');
		regex = new RegExp('eight','g');
		text = text.replace(regex, '8');
		regex = new RegExp('nine','g');
		text = text.replace(regex, '9');
		regex = new RegExp('zero','g');
		text = text.replace(regex, '0');
		regex = new RegExp('oh','g');
		text = text.replace(regex, '0');
		regex = new RegExp('for','g');
		text = text.replace(regex, '4');
		regex = new RegExp('to','g');
		text = text.replace(regex, '2');
		return text;
	}
	return text;
}

function parseFirstThree(fulfillment, filledParams) {
	console.log('*** in parseFirstThree; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseFirstThree; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseFirstThree; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseFirstThree; cleaned up text matched the RegEx; accepting');
		filledParams.firstThree = text;
	} else {
		console.log('*** in parseFirstThree; cleaned up text did not matched the RegEx; rejecting');
		filledParams.firstThree = undefined;
	}
	
	
	console.log('*** in parseFirstThree; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}



function getNextForCompoundSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'get_compound_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundRemainingSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_remaining_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'remaining_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundFirstThreeAlphas(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_first_three_alphas');
	console.log('**** in getNextForCompoundFirstThreeAlphas: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'first_three_alphas', params);
	}
	return getRemainingSerialNumberIntent(fulfillment);
}


/**
  Returns the name of the intent to transition to when the current intent ('get_serial_number')
  errors out.
  
  Per call flow logic, allow 1 combined error -- i.e. send the caller to 'contains_alpha' 
  to try to gather serial number by parts; on a third error, go to 'Default Error Intent'
*/
function getNextIntentForMaxErrors(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_serial_number_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number_errors', numErrors + 1);
	var lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (lang == 'en-us') {
		return 'first_three_alphas';
	}
	return 'contains_alphas';
}


function getNextIntentForContainsAlphas(fulfillment) {
	const response = df_fulfillment.getFilledRequiredParameter(fulfillment, 'yesNo', 'contains_alphas');
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode') || 'de-de';
	// get the value of 'yesNo' parameter filled in 'contains_alphas' intent
	if (response == 'yes') {
		if (languageBelgiumDutchDigitStrings(lang)) {
			return 'end_conversation';
		} else {
			return 'get_serial_number';
		}
		
	}
	return getNumericSerialNumberIntentName(fulfillment);
}


function getMaxErrorsNextForContainsAlphas(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'contains_alphas_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'contains_alphas_errors', numErrors + 1);
	var lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageBelgiumDutchDigitStrings(lang)) {
		return 'numeric_serial_number';
	}
	return 'get_serial_number';
}


function setSerialNumberParamAndReturnEndConversation (fulfillment) {
	const serialNumber = df_fulfillment.getFilledRequiredParameter(fulfillment, 'serialNumber', 'compound_numeric_serial_number');
	console.log('*** serialNumber collected in compound_numeric_serial_number: ' + serialNumber);
	var getSerialNumberParams = {'serialNumber': serialNumber};
	df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number', getSerialNumberParams);
	console.log('*** fulfillment after setting collected compound_numeric_serial_number: ' + JSON.fulfillment);
	return 'end_conversation';
}

function getMaxErrorsNextForNumericSerialNumber(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_numeric_serial_number_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_numeric_serial_number_errors', numErrors + 1);
	return 'Default Error Intent';
}

function getMaxErrorsNextForFirstThreeAlphas(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_first_three_alphas_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_first_three_alphas_errors', numErrors + 1);
	return 'Default Error Intent';
}

function getMaxErrorsNextForRemainingDigits(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_remaining_digits_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_remaining_digits_errors', numErrors + 1);
	return 'Default Error Intent';
}







exports.createDialogConfig = createDialogConfig;

