	'use strict';

const functions = require('firebase-functions');
const dialogflowFulfillment = require('./dialogflow-fulfillment-builder');


const df_fulfillment = require('./state-machine/fulfillment');
const snr = require('./snr');
const xps = require('./xps');
const breakfix = require('./breakfix');



const config = {
    'platformsEnabled': ['TEXT'] // define the platforms to be supported as per requirement
};


function isXPSLanguage(lang) {
	return lang == 'nl-be' || lang == 'fr-fr' || lang == 'it-it' || lang == 'de-de' || lang == 'en-gb';
}

function isSNRLanguage(lang) {
	return (lang == 'nl-nl') || (lang == 'es-es') || (lang == 'pt-pt') || (lang == 'sv-se') || (lang == 'da-dk') || (lang == 'nb-no');
}

function isBreakFixLanguage(lang) {
	return (lang == 'en-us');
}


function createDialogConfig (fulfillment)  {
	
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	console.log('**** createDialogConfig called; lang = ' + lang);
	if (isXPSLanguage(lang)) {
		return xps.createDialogConfig(fulfillment);
	}
	
	if (isSNRLanguage(lang)) {
		return snr.createDialogConfig(fulfillment);
	}
	
	if (isBreakFixLanguage(lang)) {
		return breakfix.createDialogConfig(fulfillment);
	}
	
	console.log('&&& createDialogConfig; ERROR: unsupported language = ' + lang);
	return {};	
	
}







/**
 Used fo debugging purposes only
 */
function getConversationId(request) {
	try {
	const conversationID = request.body.originalDetectIntentRequest.payload['Genesys-Conversation-Id'];
	return conversationID;
	} catch (e) {
		console.log('*** Could not get conv id' + e.message);
		return '';
	}
}

function getLanguageCode(fulfillment) {
	var lang = '';
	var welcome = fulfillment.getContext('welcome');
	if (welcome && welcome.parameters) {
		lang = welcome.parameters.LanguageCode || '';
	}
	return lang;
}

function handleRequest(request, response) {
	
		var df = new dialogflowFulfillment(config, request.body);
		
		console.log('*** conversationID: ' + JSON.stringify(getConversationId(request)));
		var intentDisplayName = df_fulfillment.getIntentName(df);
		
		var lang;
		if (intentDisplayName == 'default_welcome_intent') {
			lang = getLanguageCode(df);
			const intent = df_fulfillment.getParameter(df, 'intent_name');
			const num_retries = df_fulfillment.getParameter(df, 'num_retries');
			const num_disconfirms = df_fulfillment.getParameter(df, 'num_disconfirms');
			df_fulfillment.setGlobalParameter(df, 'languageCode', lang);
			df_fulfillment.setGlobalParameter(df,  'intent_name', intent);
			df_fulfillment.setGlobalParameter(df,  'num_retries', num_retries);
			df_fulfillment.setGlobalParameter(df,  'num_disconfirms', num_disconfirms);
		    console.log('*** in welcome; found language: ' + lang);
				
		} else {
			lang = df_fulfillment.getGlobalParameter(df, 'languageCode') || 'en-US';
		}
		
		df_fulfillment.setDialogConfig(createDialogConfig(df));
		
		if (intentDisplayName != 'BaseMenu' && intentDisplayName != 'BaseMenuXPS' && intentDisplayName != 'USA3Menu' && lang != 'en-gb') {
			df_fulfillment.setBargeIn(df, false);
		}
	
		if (intentDisplayName == 'Default Fallback Intent') {
			var name = df_fulfillment.getLastIntentName(df);
				if (name == 'BaseMenu') {
					console.log('*** caught nomatch in BaseMenu' );
					var lang = getLanguageCode(df);
					var nextIntent = "BaseMenu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'base_menu_errors') || 1;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'base_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'BaseMenuXPS') {
					console.log('*** caught nomatch in BaseMenuXPS' );
					var lang = getLanguageCode(df);
					var nextIntent = "BaseMenuXPS";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'base_menuxps_errors') || 1;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'base_menuxps_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'ChaseSubMenu') {
					console.log('*** caught nomatch in ChaseSubMenu' );
					var lang = getLanguageCode(df);
					var nextIntent = "ChaseSubMenu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'chase_sub_menu_errors') || 1;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'chase_sub_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'newTechSupport') {
					console.log('*** caught nomatch in newTechSupport' );
					var lang = getLanguageCode(df);
					var nextIntent = "newTechSupport";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'new_ts_menu_errors') || 1;
					if (numErrors == 2) {
						nextIntent = 'first_three_alphas';
					}
					df_fulfillment.setGlobalParameter(df, 'new_ts_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'A3Menu') {
					console.log('*** caught nomatch in A3Menu' );
					var lang = getLanguageCode(df);
					var nextIntent = "A3Menu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'A3Menu_errors') || 1;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'A3Menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
			
		}	
				
	
		return df_fulfillment.runStateMachine(df, response);
					   
}





//exports.DEVFulfillment = functions.https.onRequest(handleRequest);

exports.UATFulfillment = functions.https.onRequest(handleRequest);

//exports.Spanish_UATFulfillment = functions.https.onRequest(handleRequest);

//exports.AF_UATFulfillment = functions.https.onRequest(handleRequest);


//exports.UATFulfillment = functions.https.onRequest(handleRequest);





