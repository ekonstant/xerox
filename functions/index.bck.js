	'use strict';

const functions = require('firebase-functions');
const dialogflowFulfillment = require('./dialogflow-fulfillment-builder');


const df_fulfillment = require('./state-machine/fulfillment');



const config = {
    'platformsEnabled': ['TEXT'] // define the platforms to be supported as per requirement
};

const get_serial_num_prompts = {
					'initial':{
						'en':'<speak> Whats the serial number of the product you are calling about?<break time="1000ms"/></speak>',
						'fr':'<speak> Pour pouvoir vous aider, j\'ai besoin du numéro de série de votre machine?<break time="1000ms"/></speak>',
						'de':'<speak> Damit ich Ihnen helfen kann, benötige ich die Seriennummer Ihres Gerätes?<break time="1000ms"/></speak>',
						'it':'<speak> Per assisterla, ho bisogno del numero di serie del suo dispositivo?<break time="1000ms"/></speak>',
						'nl':'<speak> Om u van dienst te kunnen zijn heb ik het serienummer van de machine nodig?<break time="1000ms"/></speak>',
						'es':'<speak> Para poder ayudarle necesito que me indique el número de serie de su máquina?<break time="1000ms"/></speak>',
						'sv':'<speak> För att kunna assistera er med skrivaren,  behöver jag  serie- eller maskinnumret?<break time="1000ms"/></speak>',
						'no':'<speak> For å kunne hjelpe deg må jeg få serienummeret til maskinen din?<break time="1000ms"/></speak>',
						'da':'<speak> For at hjælpe dig er jeg nødt til at få serienummeret på din maskine?<break time="1000ms"/></speak>',
						'pt':'<speak> Para o poder ajudar necessito do número de série da sua máquina?<break time="1000ms"/></speak>',
						},

					'disconfirm': {
						'en':'<speak> Whats the serial number of the product you are calling about?<break time="1000ms"/></speak>',
						'fr':'<speak> Pour pouvoir vous aider, j\'ai besoin du numéro de série de votre machine?<break time="1000ms"/></speak>',
						'de':'<speak> Damit ich Ihnen helfen kann, benötige ich die Seriennummer Ihres Gerätes?<break time="1000ms"/></speak>',
						'it':'<speak> Per assisterla, ho bisogno del numero di serie del suo dispositivo?<break time="1000ms"/></speak>',
						'nl':'<speak> Om u van dienst te kunnen zijn heb ik het serienummer van de machine nodig?<break time="1000ms"/></speak>',
						'es':'<speak> Para poder ayudarle necesito que me indique el número de serie de su máquina?<break time="1000ms"/></speak>',
						'sv':'<speak> För att kunna assistera er med skrivaren,  behöver jag  serie- eller maskinnumret?<break time="1000ms"/></speak>',
						'no':'<speak> For å kunne hjelpe deg må jeg få serienummeret til maskinen din?<break time="1000ms"/></speak>',
						'da':'<speak> For at hjælpe dig er jeg nødt til at få serienummeret på din maskine?<break time="1000ms"/></speak>',
						'pt':'<speak> Para o poder ajudar necessito do número de série da sua máquina?<break time="1000ms"/></speak>',
					},
					
					'confirm': {
						'en':'<speak> That was <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as> </prosody><break time="700ms"/>. If that\'s correct say yes or press 1. Otherwise say no or press 2.<break time="1000ms"/></speak>',
						
						'fr':'<speak> C\'était <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Si cela est correct, dites oui ou appuyez sur 1. Sinon, dites non ou appuyez sur 2.<break time="1000ms"/></speak>',
						'de':'<speak> Die Eingabe lautet <prosody rate="slow"> <say-as interpret-as = "characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Falls diese Eingabe richtig ist sagen Sie Ja oder bitte drücken Sie 1. Andernfalls sagen Sie Nein oder drücken Sie 2. <break time="1000ms"/></speak>',
						'it':'<speak> Voleva dire <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Se é l\'opzione corretta, dica SI oppure prema 1, altrimentio dica NO oppure prema 2.<break time="1000ms"/></speak>',
						'nl':'<speak> Dat was <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Indien dit juist is, zeg Ja of kies optie 1. Anders, zeg Nee of kies optie 2.<break time="1000ms"/></speak>',
						'es':'<speak> esto era <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Si esto es correcto, diga sí o pulse 1. De lo contrario, diga no o pulse 2.<break time="1000ms"/></speak>',
						'sv':'<speak> Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Om detta var korrekt, säg ja, eller tryck 1, annars säg nej, eller tryck 2.<break time="1000ms"/></speak>',
						'no':'<speak> Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Om det er korrekt si Ja eller trykk 1, om ikke si Nei eller trykk 2.<break time="1000ms"/></speak>',
						'da':'<speak> Det var <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Hvis det er korrekt, skal du sige ja eller trykke på 1. Ellers skal du sige nej eller trykke på 2.<break time="1000ms"/></speak>',
						'pt':'<speak> será então <prosody rate="slow"> <say-as interpret-as="characters"> ${serialNumber} </say-as></prosody><break time="700ms"/>. Se está correcto diga Sim or digite 1. Caso contrário diga Não ou digite 2.<break time="1000ms"/></speak>',
					}
				};
				
const sequence_prompts = {
					'initial': getSequencePrompt,

					'retry':  getSequenceRetryPrompt
					};
					// no confirm or disconfirm prompts, as confirm is false
				

				
const first_three_alphas_prompts = {
					'initial':{
						'en':'<speak> Please tell me the first 3 characters of your serial number one at a time. For example F B T.<break time="1000ms"/></speak>',
						'fr':'<speak> Veuillez me dire les 3 premiers caractères de votre numéro de série un par un. Par exemple F B T.<break time="1000ms"/></speak>',
						'de':'<speak> Bitte sagen Sie mir nacheinander die ersten 3 Buchstaben Ihrer Seriennummer. Zum Bespiel F B T.<break time="1000ms"/></speak>',
						'it':'<speak> La preghiiamo di indicare I primi 3 caratteri del numero serie uno alla volta. Per esempio F B T.<break time="1000ms"/></speak>',
						'nl':'<speak> Gelieve de eerste drie karakters uit het serienummer op te noemen. Bijvoorbeeld, F B T.<break time="1000ms"/></speak>',
						'es':'<speak> Por favor, indíqueme los primeros 3 caracteres de su número de serie, uno a uno. Por ejemplo F B T.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg de tre första  tecknen i serienumret, en i taget. Till exempel  F B T.<break time="1000ms"/></speak>',
						'no':'<speak> Venligst fortell meg de 3 første karaterne av serienummeret, en av gangen. For eksempel F B T.<break time="1000ms"/></speak>',
						'da':'<speak> Fortæl mig de første 3 tegn i dit serienummer et ad gangen. For eksempel F B T.<break time="1000ms"/></speak>',
						'pt':'<speak> Por favor diga pausamente, os 3 primeiros caractéres do seu número de série. Como por exemplo F B T.<break time="1000ms"/></speak>',
					},

					'retry':{
						'en':'<speak> Please tell me the first 3 characters of your serial number one at a time. For example F B T.<break time="1000ms"/></speak>',
						'fr':'<speak> Veuillez me dire les 3 premiers caractères de votre numéro de série un par un. Par exemple F B T.<break time="1000ms"/></speak>',
						'de':'<speak> Bitte sagen Sie mir nacheinander die ersten 3 Buchstaben Ihrer Seriennummer. Zum Bespiel F B T.<break time="1000ms"/></speak>',
						'it':'<speak> La preghiiamo di indicare I primi 3 caratteri del numero serie uno alla volta. Per esempio F B T.<break time="1000ms"/></speak>',
						'nl':'<speak> Gelieve de eerste drie karakters uit het serienummer op te noemen. Bijvoorbeeld, F B T.<break time="1000ms"/></speak>',
						'es':'<speak> Por favor, indíqueme los primeros 3 caracteres de su número de serie, uno a uno. Por ejemplo F B T.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg de tre första  tecknen i serienumret, en i taget. Till exempel  F B T.<break time="1000ms"/></speak>',
						'no':'<speak> Venligst fortell meg de 3 første karaterne av serienummeret, en av gangen. For eksempel F B T.<break time="1000ms"/></speak>',
						'da':'<speak> Fortæl mig de første 3 tegn i dit serienummer et ad gangen. For eksempel F B T.<break time="1000ms"/></speak>',
						'pt':'<speak> Por favor diga pausamente, os 3 primeiros caractéres do seu número de série. Como por exemplo F B T.<break time="1000ms"/></speak>',
					}
					
					
					// no confirm or disconfirm prompts, as confirm is false
				};
				

function getSerialNumberIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'get_serial_number';
	}
	return 'get_compound_serial_number';
}

function getSequenceIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'Sequence';
	}
	return 'CompoundSequence';
}

function getRemainingSerialNumberIntent(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsDigitStrings(lang)) {
		return 'remaining_serial_number';
	}
	return 'compound_remaining_serial_number';
}

function getFirstThreeAlphasIntentName(fulfillment) {
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (languageSupportsAlphaNumerics(lang)) {
		return 'first_three_alphas';
	}
	return 'compound_first_three_alphas';
}


function createDialogConfig ()  {
	console.log('**** Overridden createDialogConfig called');
	df_fulfillment.setCallableFuncions([getSerialNumberIntentName, getSequenceIntentName]);
	
	df_fulfillment.addContainerIntentToConfig(configureWelcome());
	
	df_fulfillment.addSimpleIntentToConfig(configureGetSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureGetCompoundSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureContainsAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundFirstThreeAlphas());
	df_fulfillment.addSimpleIntentToConfig(configureRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundRemainingSerialNumber());
	df_fulfillment.addSimpleIntentToConfig(configureSequence());
	df_fulfillment.addSimpleIntentToConfig(configureCompoundSequence());
	df_fulfillment.addSimpleIntentToConfig(configureAllDone());
	return df_fulfillment.addDefaultPropertiesToConfig(configureDefaults());
	
}

function configureDefaults () {
	return	{
				globals:[],
				max_retries:2,
				max_disconfirms:2,	
				max_retries_next:'Default Error Intent',
				max_disconfirms_next:'Default Error Intent'
			};
		
}

df_fulfillment.setDialogConfig(createDialogConfig ());






		
function configureWelcome () {
	return {
		name:"default_welcome_intent",			// this has to match intent 'display name' in UI
		
		prompt: " ",	
		
		next: getNextForWelcome
	}
}

function getNextForWelcome(fulfillment) {
	console.log('*** in getNextForWelcome;');
	var welcome = fulfillment.getContext('welcome');
	var skipNewTS = false;
	var lang = '';
	var usa3menu = false;
	if (welcome && welcome.parameters) {
		console.log('*** in getNextForWelcome; welcome context is: ' + JSON.stringify(welcome));
		lang = welcome.parameters.LanguageCode || '';
		skipNewTS = welcome.parameters.skipNewTS || false;
		usa3menu = welcome.parameters.A3Menu || false;
	}
	
	if (lang == 'en-us') {
		df_fulfillment.setLanguage(fulfillment, 'en');
		console.log('*** in getNextForWelcome; lang is en-US');
		if ((usa3menu == false) && (skipNewTS == false)) {
			console.log('*** in getNextForWelcome; both usa3menu and skipNewTS are false -- returning get_serial_number');
			return 'newTechSupport';
		} else if (skipNewTS != false) {
			return 'get_serial_number';
		} else if (usa3menu != false) {
			return 'USA3Menu';
		}
	} else {
		return "BaseMenu";
	}
}
	
function languageSupportsAlphaNumerics(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('pt-pt'  == locale) || ('sv-se'  == locale) || ('nl-nl'  == locale) || ('da-dk'  == locale));
}

function languageSupportsDigitStrings(lang) {
	const locale = lang.toLowerCase();
	return !(('nb-no' == locale) || ('da-dk'  == locale));
}

function getSequenceRetryPrompt(fulfillment) {
	return '<speak> I did not understand. </speak>' + getSequencePrompt(fulfillment);
}

function getSequencePrompt(fulfillment) {
	
	
	console.log('*** in getSequencePrompt(); fulfillment = ' + JSON.stringify(fulfillment));
	var this_chunk = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
	var previous_chunk = df_fulfillment.getGlobalParameter(fulfillment, 'previous_chunk');
	var numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'chunking_errors') || 0;
	
	
	
	
	console.log(`*** in getSequencePrompt(); this_chunk = ${this_chunk}, previous_chunk = ${previous_chunk}`);
	if (numErrors != 0) { // retry anfter neative confirmation
		df_fulfillment.setGlobalParameter(fulfillment, 'this_chunk', undefined);
		df_fulfillment.setGlobalParameter(fulfillment, 'previous_chunk', undefined);
		df_fulfillment.setGlobalParameter(fulfillment, 'entire_number', undefined);
		df_fulfillment.setGlobalParameter(fulfillment, 'previous_entire_number', undefined);
		df_fulfillment.setGlobalParameter(fulfillment, 'chunking_errors', undefined);
		return '<speak> Ok, let\'s try one more time. Please tell me your serial number, a few digits at a time.</speak>';
	}
	if (!this_chunk && !previous_chunk) { // first time
		return '<speak> Please tell me your serial number, a few digits at a time. I will read them back to you. If I did not get it right, say "that\'s not correct". Or say "that\'s all" if you are finished. <break time="500ms"/> Please tell me your first set of digits now, for example "1 B 9".</speak>';
	}
	if ((df_fulfillment.getLastIntentName(fulfillment) == 'Sequence - edit') || (df_fulfillment.getLastIntentName(fulfillment) == 'CompoundSequence - edit')) {
		console.log('*** in getSequencePrompt(); last intent was edit');
		var prompt = '';
		if (previous_chunk) {
			 prompt =  '<speak> Lets try again. What comes after <prosody rate="slow"> <say-as interpret-as="characters"> ' + previous_chunk.split('').join(' ') + '</say-as> </prosody>?  </speak>';
		} else {
			prompt =  `<speak> Lets try again. What are the first 3 digits? </speak>`;
		}
		
		df_fulfillment.setGlobalParameter(fulfillment, 'this_chunk', previous_chunk);
		
		var previous_entire_number = df_fulfillment.getGlobalParameter(fulfillment, 'previous_entire_number') || '';
		df_fulfillment.setGlobalParameter(fulfillment, 'entire_number', previous_entire_number);
		return prompt;
	} else {
		console.log('*** in getSequencePrompt(); last intent was not edit');
		var entire_number = df_fulfillment.getGlobalParameter(fulfillment, 'entire_number') || '';
		df_fulfillment.setGlobalParameter(fulfillment, 'previous_entire_number', entire_number);
		entire_number = entire_number + this_chunk;
		var numBatch = df_fulfillment.getGlobalParameter(fulfillment, 'numBatch') || 0;
		var prompt = '';
		if (numBatch == 0) {
			prompt = '<speak> I got <prosody rate="slow"> <say-as interpret-as="characters">' + this_chunk.split('').join(' ') + '</say-as> </prosody>. Say the next few digits to continue.<break time="500ms"/> Or say "that\'s not right" to correct me. </speak>'
			df_fulfillment.setGlobalParameter(fulfillment, 'numBatch', numBatch + 1)
		} else {
			prompt = '<speak> Ok, <prosody rate="slow"> <say-as interpret-as="characters">' + this_chunk.split('').join(' ') + ' </say-as> </prosody>. Please continue. </speak>';
		}
		df_fulfillment.setGlobalParameter(fulfillment, 'entire_number', entire_number);
		return prompt;
		
		
		
	}
}

function configureGetSerialNumber () {
	return	{
				intentName: 'get_serial_number',
				paramName: 'serialNumber', 
				confirm: true,
				prompts: get_serial_num_prompts,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				max_retries:0,
				max_disconfirms:1,
				next: 'end_conversation',
				max_retries_next: getNextIntentForMaxErrors,
				max_disconfirms_next: getNextIntentForMaxErrors,
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function configureSequence () {
	return	{
				intentName: 'Sequence',
				paramName: 'sequence', 
				confirm: false,
				prompts: sequence_prompts,
				postRecoFilter: extractSequence,
				next: 'Sequence',
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function configureCompoundSequence () {
	return	{
				intentName: 'CompoundSequence',
				paramName: 'sequence', 
				confirm: false,
				prompts: sequence_prompts,
				postRecoFilter: extractCompoundSequence,
				next: 'CompoundSequence',
				retryHandler: nomatchHandlerForCompoundSequence,
			};
		
}

function nomatchHandler(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandler; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",serialNumber:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForCompoundSequence(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForCompoundSequence; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandlerForCompoundSequence; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForCompoundSequence; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_ ]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandlerForCompoundSequence; cleaned up text is: ' + text);
	regex = new RegExp('^[a-zA-Z0-9]{2,4}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	   
	    const this_chunk = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
		df_fulfillment.setGlobalParameter(fulfillment, 'this_chunk', text);
		df_fulfillment.setGlobalParameter(fulfillment, 'previous_chunk', this_chunk);
		console.log('*** nomatchHandlerForCompoundSequence: valid sequence found; setting this_chunk and previous_chunk; fulfillment: ' + JSON.stringify(fulfillment));
		// put the cleaned up text into 'sequence' parameter
		const params = {mode:"reco",sequence:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForCompoundSequence; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForCompoundSequence; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}
function nomatchHandlerForRemainingDigits(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForRemainingDigits; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandler; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandler; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){3,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { // it was an alpha-numeric string!
	    const d = {d:text};
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",remainingDigits:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandler; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandler; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function nomatchHandlerForFirstThreeAlphas(fulfillment, intentName, errorType, errorCounterMode, numErrors, maxErrors) {
	console.log(`*** in nomatchHandlerForFirstThreeAlphas; errorType = ${errorType}, errorCounterMode = ${errorCounterMode}, numErrors = ${numErrors}, maxErrors = ${maxErrors}`);
	console.log('*** in nomatchHandlerForFirstThreeAlphas; fulfillment: ' + JSON.stringify(fulfillment)); 
	if (errorType != 'nomatch') {
		// only overwrite nomatch handling
		console.log('*** in nomatchHandler; promptMode is not nomatch; returning');
		return false;
	}
	var text = fulfillment._request.queryResult.queryText;
	console.log('*** in nomatchHandlerForFirstThreeAlphas; queryText is: ' + text);
	
	if (!text) {
		return false;
	}
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandlerForFirstThreeAlphas; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for 3 alphas
	
	if(regex.test(text)) { // it was an alpha-numeric string!
		// put the cleaned up text into 'serialNumber' parameter
		const params = {mode:"reco",firstThree:text};
		// force the utterance to be accepted  
		df_fulfillment.setEvent(fulfillment, intentName.toUpperCase(), df_fulfillment.getLanguage(fulfillment), params);
		console.log('*** in nomatchHandlerForFirstThreeAlphas; RegExp match; set event with params: ' + JSON.stringify(params) + '; fulfillment=' + JSON.stringify(fulfillment));
		return true;
	}
	console.log('*** in nomatchHandlerForFirstThreeAlphas; regex did not match cleaned-up text; proceeding with nomatch handling as usual. ');
	return false;
}

function extractSequence(fulfillment, filledParams) {
	console.log('*** in extractSequence; filledParams: ' + JSON.stringify(filledParams));
	var sequence = filledParams.sequence;
	const this_chunk = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
	df_fulfillment.setGlobalParameter(fulfillment, 'this_chunk', sequence);
	df_fulfillment.setGlobalParameter(fulfillment, 'previous_chunk', this_chunk);
	console.log('*** exiting extractSequence; fulfillment: ' + JSON.stringify(fulfillment));
	return filledParams;
}

function extractCompoundSequence(fulfillment, filledParams) {
	console.log('*** in extractCompoundSequence; filledParams: ' + JSON.stringify(filledParams));
	filledParams = parseCompoundSequence(fulfillment, filledParams);
	var sequence = filledParams.sequence;
	if (sequence) {
		const this_chunk = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
		df_fulfillment.setGlobalParameter(fulfillment, 'this_chunk', sequence);
		df_fulfillment.setGlobalParameter(fulfillment, 'previous_chunk', this_chunk);
		console.log('*** exiting extractCompoundSequence; fulfillment: ' + JSON.stringify(fulfillment));
	}
	return filledParams;
}

function parseCompoundSequence(fulfillment, filledParams) {
	console.log('*** in parseCompoundSequence; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseCompoundSequence; queryText is: ' + text);
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_ ]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseCompoundSequence; cleaned up text is: ' + text);
	regex = new RegExp('^[a-zA-Z0-9]{2,4}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseCompoundSequence; cleaned up text matched the RegEx; accepting');
		filledParams.sequence = text;
	} else {
		console.log('*** in parseCompoundSequence; cleaned up text did not match the RegEx; rejecting');
		filledParams.sequence = undefined;
	}
	
	
	console.log('*** in parseCompoundSequence; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseSerialNumber; queryText is: ' + text);
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in nomatchHandler; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z0-9]\\s*){4,15}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.serialNumber = text;
	} else {
		console.log('*** in parseSerialNumber; cleaned up text did not match the RegEx; rejecting');
		filledParams.serialNumber = undefined;
	}
	
	/**
	const serialNumObj = filledParams.serialNumber;
		
	var serialNumString = "";
	if (serialNumObj['d']) {
		serialNumString = serialNumObj['d'];
	} else {
		for (var i = 1; i < 16; i++) {
			const a = 'a' + i;
			if (serialNumObj[a]) {
				serialNumString = serialNumString + serialNumObj[a];
			}
		}
	}
	filledParams.serialNumber = serialNumString;
	*/
	console.log('*** in parseSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function parseRemainingSerialNumber(fulfillment, filledParams) {
	console.log('*** in parseRemainingSerialNumber; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseRemainingSerialNumber; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseRemainingSerialNumber; cleaned up text is: ' + text);
	regex = new RegExp('^([0-9]\\s*){3,9}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseRemainingSerialNumber; cleaned up text matched the RegEx; accepting');
		filledParams.remainingDigits = text;
	} else {
		console.log('*** in parseRemainingSerialNumber; cleaned up text did not matched the RegEx; rejecting');
		filledParams.remainingDigits = undefined;
	}
	
	console.log('*** in parseRemainingSerialNumber; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function replaceSpelledDigits(lang, text) {
	
	if (lang == 'en-gb') {
		var regex = new RegExp('one','g');
		text = text.replace(regex, '1');
		regex = new RegExp('two','g');
		text = text.replace(regex, '2');
		regex = new RegExp('three','g');
		text = text.replace(regex, '3');
		regex = new RegExp('four','g');
		text = text.replace(regex, '4');
		regex = new RegExp('five','g');
		text = text.replace(regex, '5');
		regex = new RegExp('six','g');
		text = text.replace(regex, '6');
		regex = new RegExp('seven','g');
		text = text.replace(regex, '7');
		regex = new RegExp('eight','g');
		text = text.replace(regex, '8');
		regex = new RegExp('nine','g');
		text = text.replace(regex, '9');
		regex = new RegExp('zero','g');
		text = text.replace(regex, '0');
		regex = new RegExp('oh','g');
		text = text.replace(regex, '0');
		regex = new RegExp('for','g');
		text = text.replace(regex, '4');
		regex = new RegExp('to','g');
		text = text.replace(regex, '2');
		return text;
	}
	return text;
}

function parseFirstThree(fulfillment, filledParams) {
	console.log('*** in parseFirstThree; filledParams: ' + JSON.stringify(filledParams));
	
	var text = fulfillment._request.queryResult.queryText;
	
	console.log('*** in parseFirstThree; queryText is: ' + text);
	
	const lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	text = replaceSpelledDigits(lang, text);
	
	var regex = new RegExp('[\\W_]','g');
    text = text.replace(regex, '');
	// strip all non-word characters, including dashes and underscores
	
	console.log('*** in parseFirstThree; cleaned up text is: ' + text);
	regex = new RegExp('^([a-zA-Z]\\s*){3}$');
	// now see if the cleaned up utterance matches the RegEx for the serial number
	
	if(regex.test(text)) { 
		console.log('*** in parseFirstThree; cleaned up text matched the RegEx; accepting');
		filledParams.firstThree = text;
	} else {
		console.log('*** in parseFirstThree; cleaned up text did not matched the RegEx; rejecting');
		filledParams.firstThree = undefined;
	}
	
	
	console.log('*** in parseFirstThree; filledParams after filtering: ' + JSON.stringify(filledParams));
	return filledParams;
}

function configureGetCompoundSerialNumber () {
	return	{
				intentName: 'get_compound_serial_number',
				paramName: 'serialNumber', 
				confirm: true,
				postRecoFilter: parseSerialNumber,
				retryHandler: nomatchHandler,
				prompts: get_serial_num_prompts,
				max_retries:0,
				max_disconfirms:1,
				next: getNextForCompoundSerialNumber,
				max_retries_next: getNextIntentForMaxErrors,
				max_disconfirms_next: getNextIntentForMaxErrors,
				// the function getNextIntentForMaxErrors() keeps track of how many times
				// the call errored out in 'get_serial_number' dialog state; allows at most 2 iterations --
				// and sends the caller to 'Default Error Intent' on the 3rd.
			};
		
}

function getNextForCompoundSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'get_compound_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundRemainingSerialNumber(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_remaining_serial_number');
	console.log('**** in getNextForCompoundSerialNumber: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'remaining_serial_number', params);
	}
	return 'end_conversation';
}

function getNextForCompoundFirstThreeAlphas(fulfillment) {
	var context = df_fulfillment.getIntentContext(fulfillment, 'compound_first_three_alphas');
	console.log('**** in getNextForCompoundFirstThreeAlphas: context: ' + JSON.stringify(context));
	const params = context.parameters.requiredParameters;
	if (params) {
		df_fulfillment.setGlobalParameter(fulfillment, 'first_three_alphas', params);
	}
	return getRemainingSerialNumberIntent(fulfillment);
}


/**
  Returns the name of the intent to transition to when the current intent ('get_serial_number')
  errors out.
  
  Per call flow logic, allow 1 combined error -- i.e. send the caller to 'contains_alpha' 
  to try to gather serial number by parts; on a third error, go to 'Default Error Intent'
*/
function getNextIntentForMaxErrors(fulfillment) {
	const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'get_serial_number_errors') || 0;
	if (numErrors == 1) {
		return 'Default Error Intent';
	}
	df_fulfillment.setGlobalParameter(fulfillment, 'get_serial_number_errors', numErrors + 1);
	var lang = df_fulfillment.getGlobalParameter(fulfillment, 'languageCode');
	if (lang == 'en-us') {
		return 'first_three_alphas';
	}
	return 'contains_alphas';
}

function configureAllDone () {
	return	{
				intentName: 'AllDone',
				paramName: 'response', 
				confirm: false,
				prompts: {
					'initial':getFinalNumberPrompt,

					'retry': getFinalNumberRetryPrompt
				},
				max_retries:2,
				next: getNextIntentAllDone,
				
			};
		
}

function getNextIntentAllDone(fulfillment) {
	var response = df_fulfillment.getParameter(fulfillment, 'response');
	if (response == 'no') {
	
		const numErrors = df_fulfillment.getGlobalParameter(fulfillment, 'chunking_errors') || 0;
		if (numErrors < 2) {
			df_fulfillment.setGlobalParameter(fulfillment, 'chunking_errors', numErrors + 1);
			return 'Sequence';
		}
	}
	return 'end_conversation';
}

function getFinalNumberPrompt(fulfillment) {
	var number = df_fulfillment.getGlobalParameter(fulfillment, 'entire_number');
	if (!number) {
		number = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
	}
	return `<speak> Great! This is the number I got: <prosody rate="slow"> <say-as interpret-as="characters"> ${number} </say-as></prosody><break time="700ms"/>. Is that correct?  </speak>`;
}

function getFinalNumberRetryPrompt(fulfillment) {
	var number = df_fulfillment.getGlobalParameter(fulfillment, 'entire_number');
	if (!number) {
		number = df_fulfillment.getGlobalParameter(fulfillment, 'this_chunk');
	}
	return `<speak> Sorry I didn\'t get that. This is the number I got: <prosody rate="slow"> <say-as interpret-as="characters"> ${number} </say-as></prosody><break time="700ms"/>. If that is correct, say yes; otherwise say no . </speak>`;
}

function configureContainsAlphas () {
	return	{
				intentName: 'contains_alphas',
				paramName: 'yesNo', 
				confirm: false,
				prompts: {
					'initial':{
						'en':'<speak> Let\'s try something different. Does the first 3 characters of your serial number contain letters? Say yes or press 1. Otherwise say no or press 2.<break time="1000ms"/></speak>',
						'fr':'<speak> Essayons autre chose. Votre numéro de série est-il un mélange de lettres et de chiffres? Dites oui ou appuyez sur 1. Sinon, dites non ou appuyez sur 2 <break time="1000ms"/></speak>',
						'de':'<speak> Versuchen wir etwas anderes.  Sind die ersten 3 Zeichen Ihrer Seriennummer Buchstaben? Sagen Sie  Ja oder bitte drücken Sie 1. Andernfalls sagen Sie Nein oder drücken Sie 2. <break time="1000ms"/></speak>',
						'it':'<speak> Proviamo in un altro modo. Il numero seriale contiene lettere e numeri? Dire SI oppure premere 1 altrimenti dire NO oppure premere 2. <break time="1000ms"/></speak>',
						'nl':'<speak> Laten we het anders proberen. Bestaat uw serienummer uit letters en cijfers? Zeg Ja of kies optie 1, anders zeg Nee of kies optie 2 in het keuze menu. <break time="1000ms"/></speak>',
						'es':'<speak> Intentémoslo de otra forma. ¿Es su número de serie es una mezcla de letras y números? Diga sí o pulse 1. De lo contrario, diga no o pulse 2. <break time="1000ms"/></speak>',
						'sv':'<speak> Låt oss försöka något annat. Är serienumret en kombination av bokstäver och siffror? Säg ja, eller tryck 1, annars säg nej eller tryck 2 <break time="1000ms"/></speak>',
						'no':'<speak> La oss prøve noe annet, Er dit serienummer en blanding av bokstaver og tall? Si Ja eller trykk 1, om ikke si Nei eller trykk 2. <break time="1000ms"/></speak>',
						'da':'<speak> Lad os prøve noget andet. Er dit serienummer en blanding af bogstaver og tal? Sig ja eller tryk på 1. Hvis ikke, skal du sige nej eller trykke på 2. <break time="1000ms"/></speak>',
						'pt':'<speak> Tentemos de outra forma. O seu número de série é uma combinação de números e letras? Diga sim ou digite 1. Caso contrário Diga não ou digite 2. <break time="1000ms"/></speak>',
					},

					'retry':{
						'en':'<speak> Let\'s try something different. Does the first 3 characters of your serial number contain letters? Say yes or press 1. Otherwise say no or press 2.<break time="1000ms"/></speak>',
						'fr':'<speak> Essayons autre chose. Votre numéro de série est-il un mélange de lettres et de chiffres? Dites oui ou appuyez sur 1. Sinon, dites non ou appuyez sur 2 <break time="1000ms"/></speak>',
						'de':'<speak> Versuchen wir etwas anderes.  Sind die ersten 3 Zeichen Ihrer Seriennummer Buchstaben? Sagen Sie  Ja oder bitte drücken Sie 1. Andernfalls sagen Sie Nein oder drücken Sie 2. <break time="1000ms"/></speak>',
						'it':'<speak> Proviamo in un altro modo. Il numero seriale contiene lettere e numeri? Dire SI oppure premere 1 altrimenti dire NO oppure premere 2. <break time="1000ms"/></speak>',
						'nl':'<speak> Laten we het anders proberen. Bestaat uw serienummer uit letters en cijfers? Zeg Ja of kies optie 1, anders zeg Nee of kies optie 2 in het keuze menu. <break time="1000ms"/></speak>',
						'es':'<speak> Intentémoslo de otra forma. ¿Es su número de serie es una mezcla de letras y números? Diga sí o pulse 1. De lo contrario, diga no o pulse 2. <break time="1000ms"/></speak>',
						'sv':'<speak> Låt oss försöka något annat. Är serienumret en kombination av bokstäver och siffror? Säg ja, eller tryck 1, annars säg nej eller tryck 2 <break time="1000ms"/></speak>',
						'no':'<speak> La oss prøve noe annet, Er dit serienummer en blanding av bokstaver og tall? Si Ja eller trykk 1, om ikke si Nei eller trykk 2. <break time="1000ms"/></speak>',
						'da':'<speak> Lad os prøve noget andet. Er dit serienummer en blanding af bogstaver og tal? Sig ja eller tryk på 1. Hvis ikke, skal du sige nej eller trykke på 2. <break time="1000ms"/></speak>',
						'pt':'<speak> Tentemos de outra forma. O seu número de série é uma combinação de números e letras? Diga sim ou digite 1. Caso contrário Diga não ou digite 2. <break time="1000ms"/></speak>',
					}				
				},
				max_retries:2,
				next: getNextIntentForContainsAlphas,
				// this is a function that will be evaluated at runtime to figure out where to go next;
				// if the caller said 'yes', the call will go to 'first_three_alphas' intent;
				// if the caller said 'no', it will go back to 'get_serial_number' for another attempt
				
			};
		
}

function getNextIntentForContainsAlphas(fulfillment) {
	const response = df_fulfillment.getFilledRequiredParameter(fulfillment, 'yesNo', 'contains_alphas');
	// get the value of 'yesNo' parameter filled in 'contains_alphas' intent
	if (response == 'yes') {
		return getFirstThreeAlphasIntentName(fulfillment);
	}
	return getSerialNumberIntentName(fulfillment);
}

function configureFirstThreeAlphas () {
	return	{
				intentName: 'first_three_alphas',
				paramName: 'firstThree', 
				confirm: false,
				postRecoFilter: parseFirstThree,
				retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				next: 'remaining_serial_number',
				
			};
		
}

function configureCompoundFirstThreeAlphas () {
	return	{
				intentName: 'compound_first_three_alphas',
				paramName: 'firstThree', 
				confirm: false,
				postRecoFilter: parseFirstThree,
				retryHandler: nomatchHandlerForFirstThreeAlphas,
				prompts: first_three_alphas_prompts,
				max_retries:2,
				next: getNextForCompoundFirstThreeAlphas,
				
			};
		
}

function configureRemainingSerialNumber () {
	return	{
				intentName: 'remaining_serial_number',
				paramName: 'remainingDigits', 
				postRecoFilter: parseRemainingSerialNumber,
				retryHandler: nomatchHandlerForRemainingDigits,
				confirm: false,
				prompts: {
					'initial':{
						'en':'<speak> Now, please say or enter the remaining digits of your serial number.<break time="1000ms"/></speak>',
						'fr':'<speak> Maintenant veuillez s\'il vous plait dire ou entrer les chiffres restants de votre numéro de série.<break time="1000ms"/></speak>',
						'de':'<speak> Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.<break time="1000ms"/></speak>',
						'it':'<speak> Ora la preghiamop di inserire I numeri e/o lettere rimanenti.<break time="1000ms"/></speak>',
						'nl':'<speak> Geef nu de resterende cijfers van uw serienummer op. U kunt deze inspreken of ingeven.<break time="1000ms"/></speak>',
						'es':'<speak> Ahora, indique o  digite los números restantes de su número de serie.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg eller skriv nu in resterande siffror.<break time="1000ms"/></speak>',
						'no':'<speak> Nå, si eller tast inn de resterende nummerene i serie nummeret.<break time="1000ms"/></speak>',
						'da':'<speak> Sig venligst eller indtast de resterende cifre i dit serienummer.<break time="1000ms"/></speak>',
						'pt':'<speak> Agora, por favor diga ou digite os restantes algarismos.<break time="1000ms"/></speak>',
					},

					'retry1': {
						'en':'<speak> Now, please say or enter the remaining digits of your serial number.<break time="1000ms"/></speak>',
						'fr':'<speak> Maintenant veuillez s\'il vous plait dire ou entrer les chiffres restants de votre numéro de série.<break time="1000ms"/></speak>',
						'de':'<speak> Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.<break time="1000ms"/></speak>',
						'it':'<speak> Ora la preghiamop di inserire I numeri e/o lettere rimanenti.<break time="1000ms"/></speak>',
						'nl':'<speak> Geef nu de resterende cijfers van uw serienummer op. U kunt deze inspreken of ingeven.<break time="1000ms"/></speak>',
						'es':'<speak> Ahora, indique o  digite los números restantes de su número de serie.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg eller skriv nu in resterande siffror.<break time="1000ms"/></speak>',
						'no':'<speak> Nå, si eller tast inn de resterende nummerene i serie nummeret.<break time="1000ms"/></speak>',
						'da':'<speak> Sig venligst eller indtast de resterende cifre i dit serienummer.<break time="1000ms"/></speak>',
						'pt':'<speak> Agora, por favor diga ou digite os restantes algarismos.<break time="1000ms"/></speak>',
						// no confirm or disconfirm prompts, as confirm is false
					},
					'retry2': {
						'en':'<speak> Now, please enter the remaining digits of your serial number.<break time="1000ms"/></speak>',
						'fr':'<speak> Maintenant veuillez s\'il vous plait dire ou entrer les chiffres restants de votre numéro de série.<break time="1000ms"/></speak>',
						'de':'<speak> Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.<break time="1000ms"/></speak>',
						'it':'<speak> Ora la preghiamop di inserire I numeri e/o lettere rimanenti.<break time="1000ms"/></speak>',
						'nl':'<speak> Geef nu de resterende cijfers van uw serienummer op. U kunt deze inspreken of ingeven.<break time="1000ms"/></speak>',
						'es':'<speak> Ahora, indique o  digite los números restantes de su número de serie.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg eller skriv nu in resterande siffror.<break time="1000ms"/></speak>',
						'no':'<speak> Nå, si eller tast inn de resterende nummerene i serie nummeret.<break time="1000ms"/></speak>',
						'da':'<speak> Sig venligst eller indtast de resterende cifre i dit serienummer.<break time="1000ms"/></speak>',
						'pt':'<speak> Agora, por favor diga ou digite os restantes algarismos.<break time="1000ms"/></speak>',
						// no confirm or disconfirm prompts, as confirm is false
					}
				},
				max_retries:3,
				next: 'end_conversation',
				
			};
		
}

function configureCompoundRemainingSerialNumber () {
	return	{
				intentName: 'compound_remaining_serial_number',
				paramName: 'remainingDigits', 
				confirm: false,
				postRecoFilter: parseRemainingSerialNumber,
				retryHandler: nomatchHandlerForRemainingDigits,
				prompts: {
					'initial':{
						'en':'<speak> Now, please say or enter the remaining digits of your serial number.<break time="1000ms"/></speak>',
						'fr':'<speak> Maintenant veuillez s\'il vous plait dire ou entrer les chiffres restants de votre numéro de série.<break time="1000ms"/></speak>',
						'de':'<speak> Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.<break time="1000ms"/></speak>',
						'it':'<speak> Ora la preghiamop di inserire I numeri e/o lettere rimanenti.<break time="1000ms"/></speak>',
						'nl':'<speak> Geef nu de resterende cijfers van uw serienummer op. U kunt deze inspreken of ingeven.<break time="1000ms"/></speak>',
						'es':'<speak> Ahora, indique o  digite los números restantes de su número de serie.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg eller skriv nu in resterande siffror.<break time="1000ms"/></speak>',
						'no':'<speak> Nå, si eller tast inn de resterende nummerene i serie nummeret.<break time="1000ms"/></speak>',
						'da':'<speak> Sig venligst eller indtast de resterende cifre i dit serienummer.<break time="1000ms"/></speak>',
						'pt':'<speak> Agora, por favor diga ou digite os restantes algarismos.<break time="1000ms"/></speak>',
					},

					'retry': {
						'en':'<speak> Now, please say or enter the remaining digits of your serial number.<break time="1000ms"/></speak>',
						'fr':'<speak> Maintenant veuillez s\'il vous plait dire ou entrer les chiffres restants de votre numéro de série.<break time="1000ms"/></speak>',
						'de':'<speak> Nun, sagen oder geben Sie die restlichen Zahlen Ihrer Seriennummer ein.<break time="1000ms"/></speak>',
						'it':'<speak> Ora la preghiamop di inserire I numeri e/o lettere rimanenti.<break time="1000ms"/></speak>',
						'nl':'<speak> Geef nu de resterende cijfers van uw serienummer op. U kunt deze inspreken of ingeven.<break time="1000ms"/></speak>',
						'es':'<speak> Ahora, indique o  digite los números restantes de su número de serie.<break time="1000ms"/></speak>',
						'sv':'<speak> Vänligen säg eller skriv nu in resterande siffror.<break time="1000ms"/></speak>',
						'no':'<speak> Nå, si eller tast inn de resterende nummerene i serie nummeret.<break time="1000ms"/></speak>',
						'da':'<speak> Sig venligst eller indtast de resterende cifre i dit serienummer.<break time="1000ms"/></speak>',
						'pt':'<speak> Agora, por favor diga ou digite os restantes algarismos.<break time="1000ms"/></speak>',
						// no confirm or disconfirm prompts, as confirm is false
					}
				},
				max_retries:2,
				next: getNextForCompoundRemainingSerialNumber,
				
			};
		
}

/**
 Used fo debugging purposes only
 */
function getConversationId(request) {
	try {
	const conversationID = request.body.originalDetectIntentRequest.payload['Genesys-Conversation-Id'];
	return conversationID;
	} catch (e) {
		console.log('*** Could not get conv id' + e.message);
		return '';
	}
}

function getLanguageCode(fulfillment) {
	var lang = '';
	var welcome = fulfillment.getContext('welcome');
	if (welcome && welcome.parameters) {
		lang = welcome.parameters.LanguageCode || '';
	}
	return lang;
}

function handleRequest(request, response) {
	
		var df = new dialogflowFulfillment(config, request.body);
		
		console.log('*** conversationID: ' + JSON.stringify(getConversationId(request)));
		var intentDisplayName = df_fulfillment.getIntentName(df);
		
		if (intentDisplayName == 'default_welcome_intent') {
			var lang = getLanguageCode(df);
			df_fulfillment.setGlobalParameter(df, 'languageCode', lang);
		    console.log('*** in welcome; found language: ' + lang);
				
		}
		
		if (intentDisplayName == 'Default Fallback Intent') {
			var name = df_fulfillment.getLastIntentName(df);
				if (name == 'BaseMenu') {
					console.log('*** caught nomatch in BaseMenu' );
					var lang = getLanguageCode(df);
					var nextIntent = "BaseMenu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'base_menu_errors') || 0;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'base_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'ChaseSubMenu') {
					console.log('*** caught nomatch in ChaseSubMenu' );
					var lang = getLanguageCode(df);
					var nextIntent = "ChaseSubMenu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'chase_sub_menu_errors') || 0;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'chase_sub_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'newTechSupport') {
					console.log('*** caught nomatch in newTechSupport' );
					var lang = getLanguageCode(df);
					var nextIntent = "newTechSupport";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'new_ts_menu_errors') || 0;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'new_ts_menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
				
				if (name == 'A3Menu') {
					console.log('*** caught nomatch in A3Menu' );
					var lang = getLanguageCode(df);
					var nextIntent = "A3Menu";
					var numErrors = df_fulfillment.getGlobalParameter(df, 'A3Menu_errors') || 0;
					if (numErrors == 2) {
						nextIntent = 'Default Error Intent';
					}
					df_fulfillment.setGlobalParameter(df, 'A3Menu_errors', numErrors + 1);
					df_fulfillment.triggerIntent(df, nextIntent);
					return response.json(df.getCompiledResponse());	
				}
			
		}	
				
		
		return df_fulfillment.runStateMachine(df, response);
					   
}





exports.dialogflowFirebaseFulfillment = functions.https.onRequest(handleRequest);
exports.getSerialNumberIntentName = getSerialNumberIntentName;
exports.getSequenceIntentName = getSequenceIntentName;


